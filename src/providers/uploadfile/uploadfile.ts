import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { UserData } from '../user-data';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import 'rxjs/add/operator/map';


//import moment from 'moment';
import 'moment/locale/pt-br';
/*
  Generated class for the UploadfileProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UploadfileProvider {
  imageCache: any = [];
  userInfo: any = this.userData.getUserInfo();
  constructor(
    public userData: UserData,
    public http: Http,
    public loading: LoadingController,
    public file: File,
    public transfer: Transfer,
    public filePath: FilePath,
    public tstCtrl: ToastController
  ) {
    this.userData.getUserInfo().then(info => {
      this.userInfo = info;
    });
  }

  loader: any;
  uploadImage(name: any, model: any) {
    console.log(name + "<->" + model);
    // Destination URL
    var url = this.userData.base_url + 'api/projects';

    // File for Upload
    var targetPath = this.pathForImage(model);


    var options = {
      fileKey: name,
      fileName: model,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: {
        'fileName': model,
        'AuthKey': this.userInfo.user_key
      }
    };
    console.log("------------------");
    console.log(JSON.stringify(options));
    console.log("------------------");
    const fileTransfer: TransferObject = this.transfer.create();

    this.loader = this.loading.create({
      content: 'Uploading...',
    });
    this.loader.present();

    // Use the FileTransfer to upload the image

    fileTransfer.upload(targetPath, url, options).then(data => {
      this.loader.dismissAll();
      console.log("=====================Um Here=======================");
      console.log(JSON.stringify(data));
      this.presentToast(name + ' succesfully uploaded.');
      console.log("=====================Um Here=======================");
    }, err => {
      this.loader.dismissAll()
      console.log(JSON.stringify(err));
      this.presentToast(JSON.stringify(err));
    });
  }

  uploadImageWithPath(name: any, model: any, path: any) {
    console.log('-----name', name);
    console.log('-----model', model);
    // Destination URL
    var url = this.userData.base_url + 'api/modules';

    // File for Upload
    var targetPath = path;

    var options = {
      fileKey: name,
      fileName: model,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: {
        'fileName': model,
        '_uploadFiles': 1,
        'AuthKey': this.userInfo.user_key
      }
    };
    const fileTransfer: TransferObject = this.transfer.create();

    this.loader = this.loading.create({
      content: 'Uploading...' + name,
    });
    this.loader.present();
    // Use the FileTransfer to upload the image
    var imagePromise = new Promise((resolve, reject) => {
      fileTransfer.upload(targetPath, url, options).then(data => {
        if (data != undefined) {
          console.log("=====================Um Here=======================");
          console.log('response', JSON.stringify(data.response));
          let resp = JSON.parse(data.response);
          if (resp != null) {
            let fileName = resp.response.fileName[0].value.toString();
            console.log('json response', fileName);
            resolve(fileName);
          }
          console.log("=====================Um Here=======================");
        }
        this.loader.dismissAll();
      }, (err) => {
        this.presentToast(JSON.stringify(err));
        console.error(JSON.stringify(err));
        this.loader.dismissAll();
        reject();
      });
    });

    return imagePromise.then((data) => {
      console.log('Got data! Promise fulfilled.');
      return data;
    }, (error) => {
      console.log('Promise rejected.');
      console.log(error.message);
      return null;
    });
  }

  uploadImageWithPathOnce(images: any, form: any) {
    const url = this.userData.base_url + 'api/modules';
    let fileTransfer: TransferObject = this.transfer.create();
    const promisesArray: any[] = [];
    images.forEach(element => {
      promisesArray.push(new Promise((resolve, reject) => {
        this.loader = this.loading.create({
          content: 'Uploading...' + element.name,
        });
        this.loader.present();
        var options = {
          fileKey: element.name,
          fileName: element.model,
          chunkedMode: false,
          mimeType: "multipart/form-data",
          params: {
            'fileName': element.model,
            '_uploadFiles': 1,
            'AuthKey': this.userInfo.user_key
          }
        };
        fileTransfer.upload(element.path, url, options).then(async (data) => {
          if (data != undefined) {
            // console.log("=====================Um Here=======================");
            // console.log('response', JSON.stringify(data.response));
            // console.log("=====================Um Here=======================");
            let resp = await JSON.parse(data.response);
            if (resp != null) {
              let fileName = resp.response.fileName[0].value.toString();
              let json = {
                fileName: fileName,
                name: element.name
              };
              this.imageCache.push(json);
              // console.log('json response >>>', JSON.stringify(json));
              resolve(json);
            }
          }
        }).catch(err => {
          console.log('Promise rejected.');
          console.log(err.message);
          this.loader.dismissAll();
          reject();
        })
      }).then((data: any) => {
        console.log('Promise one Fullfilled.',JSON.stringify(data));
        let tempName = form[data.name];
        if (tempName.includes('file'))
          form[data.name] = data.fileName;
        else
          form[data.name] = form[data.name] + "," + data.fileName;
        // console.log('Update Form', JSON.stringify(form));
      }))
    });

    setTimeout(() => {
      this.imageCache.forEach(element => {
        form[element.name] = element.fileName;
      });
      this.loader.dismissAll();
      return form;
    }, 10000);

    let forms = Promise.all(promisesArray).then(() => {
      // console.log('Promises done', JSON.stringify(form));
      // console.log('image cache', JSON.stringify(this.imageCache));
      this.loader.dismissAll();
      return form;
    }).catch(e => {
      console.log('err', JSON.stringify(e));
    });
    console.log('forms', JSON.stringify(forms));
    return forms;
  }

  // Show tost message
  private presentToast(text) {
    let toast = this.tstCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return this.file.dataDirectory + img;
    }
  }

}
