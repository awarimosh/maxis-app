import { Injectable } from '@angular/core';
import { ToastController, Platform } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { AlertController } from 'ionic-angular';
//import { Observable } from 'rxjs/Rx';

import moment from 'moment';
import 'moment/locale/pt-br';

@Injectable()
export class UserData {
  _favorites: string[] = [];
  HAS_LOGGED_IN = 'hasLoggedIn';
  HAS_SEEN_TUTORIAL = 'hasSeenTutorial';
  USER_INFO = '';
  USER_DOMAIN = '';

  disconnectSubscription: any;
  connectSubscription: any;
  connectMsg = "Connected!";
  isConnected = true;
  //base_url = "http://172.20.10.2/";  //mobile network
  //base_url = "http://192.168.1.10/";  //office network
  //base_url = "http://192.168.0.102/"; //home network
  //base_url = "http://localhost/";
  base_url = "https://apps.izeberg.com/maxis_osp/"; //live server

  sqlConf: any;
  constructor(
    public events: Events,
    public storage: Storage,
    public http: Http,
    public network: Network,
    public tstCtrl: ToastController,
    public sqlite: SQLite,
    public platform: Platform,
    public alertCtrl: AlertController,
  ) {

    //Database settings
    if (this.platform.is('ios')) {
      this.sqlConf = {
        name: 'maxis.db',
        location: 'default'
      };
    }
    else {
      this.sqlConf = {
        name: 'maxis.db',
        iosDatabaseLocation: 'Documents'
      };
    }
  }

  /* let sub: any = 
        Observable.interval(10000)
          .subscribe((val) => { console.log('called'); } */

  hasFavorite(sessionName: string): boolean {
    return (this._favorites.indexOf(sessionName) > -1);
  };

  addFavorite(sessionName: string): void {
    this._favorites.push(sessionName);
  };

  removeFavorite(sessionName: string): void {
    let index = this._favorites.indexOf(sessionName);
    if (index > -1) {
      this._favorites.splice(index, 1);
    }
  };

  login(username: string): void {
    this.storage.set(this.HAS_LOGGED_IN, true);
    this.setUsername(username);
    this.events.publish('user:login');
  };

  saveUserEmail(userEmail: any): void {
    this.storage.set('userEmail', userEmail);
  }

  saveUserInfo(userInfo: any): void {
    this.setUserInfo(userInfo);
  };

  saveSystemUsers(users: any): void {
    this.storage.set('systemUsers', users);
  };

  saveDomain(domain: any): void {
    this.storage.set('domain', domain);
    this.USER_DOMAIN = domain;
  }

  signup(username: string): void {
    this.storage.set(this.HAS_LOGGED_IN, true);
    this.setUsername(username);
    this.events.publish('user:signup');
  };

  logout(): void {
    this.storage.remove(this.HAS_LOGGED_IN);
    this.storage.remove('username');
    this.storage.remove('userInfo');
    this.storage.remove('hasLoggedIn');
    this.USER_INFO = null;
    this.events.publish('user:logout');
  };

  setUsername(username: string): void {
    this.storage.set('username', username);
  };

  getUsername(): Promise<string> {
    return this.storage.get('username').then((value) => {
      return value;
    });
  };

  setUserInfo(userInfo: any): void {
    this.storage.set('userInfo', userInfo);
    this.USER_INFO = userInfo;
  };

  getUserInfo(): Promise<string> {
    return this.storage.get('userInfo').then((value) => {
      return value;
    });
  };

  hasLoggedIn(): Promise<boolean> {
    return this.storage.get(this.HAS_LOGGED_IN).then((value) => {
      return value === true;
    });
  };

  checkHasSeenTutorial(): Promise<string> {
    return this.storage.get(this.HAS_SEEN_TUTORIAL).then((value) => {
      return value;
    });
  };s

  presentToast(text) {
    let toast = this.tstCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  presentLongToast(text) {
    let toast = this.tstCtrl.create({
      message: text,
      duration: 6000,
      position: 'top'
    });
    toast.present();
  }

  watchNetwork() {
    // watch network for a disconnect
    this.disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
      this.connectMsg = 'network was disconnected :-(';
      alert(this.connectMsg);
      this.isConnected = false;
      this.syncCustomForm();
    });


    // watch network for a connection
    this.connectSubscription = this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      this.isConnected = true;
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        if (this.network.type === 'wifi') {
          console.log('we got a wifi connection, woohoo!');
          this.connectMsg = 'we got a wifi connection, woohoo!';
          alert(this.connectMsg);
          this.syncCustomForm();
        }
      }, 3000);
    });
  }

  userInfo: any;
  moduleIds = {
    'projects': 9,
    'site_instruction_form': 100,
    'civil': 103,
    "cable": 104,
    "e2e": 105,
    "site_clearance": 106,
    "eot": 99
  };
  moduleSettings: any = {};
  getCustomForms() {
    this.storage.get('userInfo').then((value) => {
      this.userInfo = value;
    }).then(() => {
      for (let key in this.moduleIds) {
        var body = 'module_id=' + this.moduleIds[key] + '&_getModuleSettings=1';
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        this.http.post(this.base_url + 'api/modules?AuthKey=' + this.userInfo.user_key,
          body, {
            headers: headers
          })
          .map(res => res.json())
          .subscribe(data => {
            if (data != undefined) {
              data = data.response.data;
              this.saveToStorage('moduleSettings', JSON.stringify(data));
              //this.moduleAttributesList = this.civilForm;
              this.moduleSettings[key] = data;
              console.log('form retrieved ' + key, data);
            }
          }, error => {
            console.log('1', JSON.stringify(error));
          });
      }
    }).then(() => {
      console.log('moduleIds ', this.moduleIds);
      console.log('moduleSettings ', this.moduleSettings);
    });

  }

  getCustomFormsOffline() {
    this.storage.get('userInfo').then((value) => {
      this.userInfo = value;
    }).then(async () => {
      for (let key in this.moduleIds) {
        let data = await this.getFromStorage('moduleSettings');
        //this.moduleAttributesList = this.civilForm;
        this.moduleSettings[key] = JSON.parse(data);
        // console.log('form retrieved ' + key, data);
      }
    }).then(() => {
      console.log('moduleIds ', this.moduleIds);
      console.log('moduleSettings ', this.moduleSettings);
    });

  }

  saveToStorage(key: string, obj: any) {
    // var url = 'data:text/json;charset=utf8,' + encodeURIComponent(JSON.stringify(obj));
    // console.log('saved projects',url);
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.storage.set(key, obj);
    }
  }

  async getFromStorage (key: string) {
    var result;
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      result = await this.storage.get(key).then((val) => {
        return val;
      });
    }
    else
      result = null;
    return result;
  }

  civilForm = "civil form";

  syncArray: any = [];
  lastUpdatedDate: any;

  // if (this.activity.activity_update_local.length == 0) {
  //   this.activity.activity_update_local.push(result.rows.item(i));
  // } else {
  //   if (result.rows.item(i).form_custom_fields != null) {
  //     for (var j = 0; j < this.activity.activity_update_local.length; j++) {
  //       if (result.rows.item(i).form_id == this.activity.activity_update_local[j].form_id) {
  //         break
  //       }
  //       else if (j == this.activity.activity_update_local.length - 1) {
  //         this.activity.activity_update_local.push(result.rows.item(i));
  //       }
  //     }
  //   }
  syncCustomFormOne(formID: string) {
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.sqlite.create(this.sqlConf)
        .then((db: SQLiteObject) => {
          db.executeSql("SELECT * FROM customForm WHERE form_sync = 0 AND form_module_id = " + formID, [])
            .then((result) => {
              if (result.rows.length > 0) {
                for (var i = 0; i < result.rows.length; i++) {
                  this.syncArray.push(result.rows.item(i));
                }
                if (this.isConnected) {
                  this.updateServer(JSON.stringify(this.syncArray));
                } else {
                  this.presentToast(result.rows.length + ' item pending sync');
                }
                var url = 'data:text/json;charset=utf8,' + encodeURIComponent(JSON.stringify(this.syncArray));
                console.log('syncArray', url);
              }
            }).catch(e => {
              let alert = this.alertCtrl.create({
                title: 'Alert 3',
                subTitle: JSON.stringify(e),
                buttons: ['OK']
              });
              alert.present();
            })
        })
        .catch(e => {
          let alert = this.alertCtrl.create({
            title: 'Alert 4',
            subTitle: JSON.stringify(e),
            buttons: ['OK']
          });
          alert.present();
        })
    }
  }

  syncCustomForm() {
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.sqlite.create(this.sqlConf)
        .then((db: SQLiteObject) => {
          db.executeSql("SELECT * FROM customForm WHERE form_sync = 0", [])
            .then((result) => {
              if (result.rows.length > 0) {
                for (var i = 0; i < result.rows.length; i++) {
                  this.syncArray.push(result.rows.item(i));
                }
                if (this.isConnected) {
                  this.updateServer(JSON.stringify(this.syncArray));
                } else {
                  this.presentToast(result.rows.length + ' item pending sync');
                }
              }
            }).catch(e => {
              let alert = this.alertCtrl.create({
                title: 'Alert 3',
                subTitle: JSON.stringify(e),
                buttons: ['OK']
              });
              alert.present();
            })
        })
        .catch(e => {
          let alert = this.alertCtrl.create({
            title: 'Alert 4',
            subTitle: JSON.stringify(e),
            buttons: ['OK']
          });
          alert.present();
        })
    }
  }

  /************************* Update server with local data ************
  *********************************************************************
  *********************************************************************/

  updateServer(data: any) {
    var body = 'AuthKey=' + this.userInfo.user_key + '&data=' + data + '&_updateServer=1';
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    this.http.post(this.base_url + 'api/modules',
      body, {
        headers: headers
      })
      .map(res => res.json())
      .subscribe(resData => {
        if (resData != undefined) {
          let innerData = resData.response.data;
          innerData.forEach(element => {
            if (element.module_response != null)
              this.updateSyncID(element.record_id, element.module_response.response.inserted_id);
          });
        }
      }, error => {
        console.log('2', JSON.stringify(error));
      });
  }

  updateSyncID(primVal: any, serverRecordId: any) {
    // console.log('updateSyncID',JSON.stringify(primVal));
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.sqlite.create(this.sqlConf)
        .then((db: SQLiteObject) => {
          //data insert section
          db.executeSql("UPDATE customForm SET form_id=?,form_sync=1 WHERE form__id = ?",
            [serverRecordId, primVal])
            // .then((result) => {
            //   console.log(primVal + "<<->>" + serverRecordId + "<<->>" + JSON.stringify(result));
            // })
            .catch(error => {
              console.log('3', JSON.stringify(error));
            });
        })
        .catch(e => alert(JSON.stringify(e)));
    }
  }

  updateTable(date: any) {
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.sqlite.create(this.sqlConf)
        .then((db: SQLiteObject) => {
          db.executeSql("INSERT INTO updateTbl (update_on) " +
            "VALUES(?)", [date])
            .then(() => {
              console.log('inserted successfully ' + date);
            })
            .catch(error => {
              console.log('4', JSON.stringify(error));
            });
        })
        .catch(e => alert(JSON.stringify(e)));
    }
  }

  truncateTable(tblName: any) {
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.sqlite.create(this.sqlConf)
        .then((db: SQLiteObject) => {
          db.executeSql("DELETE FROM " + tblName, [])
            .then((result) => {
              console.log("deleted: " + JSON.stringify(result));
            })
            .catch(error => {
              console.log('5', JSON.stringify(error));
            });
        })
        .catch(e => alert(JSON.stringify(e)));
    }
  }
  /************************* Update local with server data ************
  *********************************************************************
  *********************************************************************/

  updateLocal() {
    /* this.truncateTable('customForm');
    return false; */
    //var body = 'AuthKey='+this.userInfo.user_key+'&list_module_sync='+JSON.stringify(this.moduleIds)+'&lastUpdateDate='+this.lastUpdatedDate+'&_syncFromServer=1';
    if (typeof (this.lastUpdatedDate) == 'undefined') {
      this.lastUpdatedDate = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    }
    console.log(this.lastUpdatedDate);
    var body = 'AuthKey=' + this.userInfo.user_key + '&list_module_sync=' + JSON.stringify(this.moduleIds) + '&lastUpdateDate=' + this.lastUpdatedDate + '&_syncFromServer=1';
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    /* console.log(body);
    return false; */
    this.http.post(this.base_url + 'api/modules',
      body, {
        headers: headers
      })
      .map(res => res.json())
      .subscribe(data => {
        if (data != undefined) {
          data = data.response.data;
          Object.keys(data).forEach(key => {
            this.saveLocalForm(data[key], key);
          });
        }
      }, error => {
        console.log('6', JSON.stringify(error));
      });
  }

  saveLocalForm(data: any, module_key: string) {
    if (!data) {
      return false;
    }
    let colList: string = "";
    let colListVal: string = "";
    Object.keys(data[0]).forEach(k => {
      if (colList == "") {
        colList += k.replace(module_key, "form");
        colListVal += "?";
      } else {
        colList += "," + k.replace(module_key, "form");
        colListVal += ",?";
      }
    });
    let colData: any;
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.sqlite.create(this.sqlConf)
        .then((db: SQLiteObject) => {
          for (var i = 0; i < data.length; i++) {
            colData = [];

            Object.keys(data[i]).forEach(k => {
              colData.push(data[i][k]);
            });

            db.executeSql("INSERT INTO customForm (form_sync, form_module_key, form_module_id," + colList + ") " +
              "VALUES(1,'" + module_key + "','" + this.moduleIds[module_key] + "'," + colListVal + ")", colData)
              .then(() => {
                console.log('insert successfully: ' + JSON.stringify(colData));
                this.lastUpdatedDate = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
                this.updateTable(this.lastUpdatedDate);
              })
              .catch(error => {
                console.log('7', JSON.stringify(error));
              });
          }
        })
        .catch(e => alert(JSON.stringify(e)));
    }
  }

}
