import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { UserData } from '../user-data';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import 'rxjs/add/operator/map';

/*
  Generated class for the TakePhotoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TakePhotoProvider {

  constructor(
              public userData: UserData,
              public http: Http,
              public camera: Camera,
              public file: File,
              public loading: LoadingController,
              public tstCtrl: ToastController
            ) {
  }

  // Create a new name for the image
  createFileName(name: any) {
    var d = new Date(),
    n = d.getTime(),
    newFileName = name + n + ".jpg";
    return newFileName;
  }
  
  // Copy the image to a local folder
  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      console.log(JSON.stringify(success));
      this.userData.presentToast('Photo captured sucessfully! '+ newFileName);
    }, error => {
      console.log(error);
      this.userData.presentToast('Error while storing file.');
    });
  }
}
