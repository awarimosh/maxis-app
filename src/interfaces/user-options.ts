
export interface UserOptions {
  username: string,
  password: string,
  siteName: string
}
