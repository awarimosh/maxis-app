import { Component, ViewChild } from '@angular/core';
import { Keyboard } from '@ionic-native/keyboard';
import { Events, MenuController, Nav, Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import { Storage } from '@ionic/storage';
import { OneSignal } from '@ionic-native/onesignal';

import { AboutPage } from '../pages/about/about';
//import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
//import { MapPage } from '../pages/map/map';
import { SignupPage } from '../pages/signup/signup';
import { TabsPage } from '../pages/tabs-page/tabs-page';
import { TutorialPage } from '../pages/tutorial/tutorial';
/* import { SchedulePage } from '../pages/schedule/schedule'; */
//import { SpeakerListPage } from '../pages/speaker-list/speaker-list';
//import { SupportPage } from '../pages/support/support';
import { ProjectsPage } from '../pages/projects/projects';
import { EotPage } from '../pages/eot/eot';
import { SiteInstructionPage } from '../pages/site-instruction/site-instruction';
//import { MyTaskPage } from '../pages/my-task/my-task';


import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';

import 'rxjs/add/operator/map';
import { DashboardPage } from '../pages/dashboard/dashboard';
//import { Observable } from 'rxjs/Rx';

/* import moment from 'moment';
import 'moment/locale/pt-br'; */


export interface PageInterface {
  title: string;
  name: string;
  component: any;
  icon: string;
  logsOut?: boolean;
  index?: number;
  tabName?: string;
  tabComponent?: any;
}

@Component({
  templateUrl: 'app.template.html'
})
export class ConferenceApp {
  // the root nav is a child of the root app component
  // @ViewChild(Nav) gets a reference to the app's root nav
  @ViewChild(Nav) nav: Nav;

  // List of pages that can be navigated to from the left menu
  // the left menu only works after login
  // the login page disables the left menu
  appPages: PageInterface[] = [
    /* { title: 'My Tasks', name: 'TabsPage', component: TabsPage, tabComponent: MyTaskPage,      index: 0, icon: 'list-box' }, */
    { title: 'Projects', name: 'TabsPage', component: TabsPage, tabComponent: ProjectsPage, index: 0, icon: 'briefcase' },
    { title: 'Dashboard', name: 'TabsPage', component: TabsPage, tabComponent: DashboardPage, index: 4, icon: 'pie' },
    { title: 'Site Ins. Form', name: 'TabsPage', component: TabsPage, tabComponent: SiteInstructionPage, index: 1, icon: 'ios-information-circle' },
    { title: 'EOT', name: 'TabsPage', component: TabsPage, tabComponent: EotPage, index: 3, icon: 'ios-clock' },
    { title: 'Profile', name: 'TabsPage', component: TabsPage, tabComponent: AboutPage, index: 2, icon: 'information-circle' }
    // { title: 'Technical Proposal', name: 'TabsPage', component: TabsPage, tabComponent: ProjectsPage,    index: 5, icon: 'ios-document' },
    /* { title: 'Calendar', name: 'TabsPage', component: TabsPage, tabComponent: SpeakerListPage, index: 2, icon: 'contacts' }, */
    /* { title: 'Map',      name: 'TabsPage', component: TabsPage, tabComponent: MapPage,         index: 3, icon: 'map' }, */
    /* { title: 'Profile',  name: 'TabsPage', component: TabsPage, tabComponent: AboutPage,       index: 4, icon: 'information-circle' } */
  ];
  loggedInPages: PageInterface[] = [
    /* { title: 'Account', name: 'AccountPage', component: AccountPage, icon: 'person' },
    { title: 'Support', name: 'SupportPage', component: SupportPage, icon: 'help' }, */
    { title: 'Logout', name: 'LoginPage', component: LoginPage, icon: 'log-out', logsOut: true }
  ];
  loggedOutPages: PageInterface[] = [
    { title: 'Login', name: 'LoginPage', component: LoginPage, icon: 'log-in' },
    /* { title: 'Support', name: 'SupportPage', component: SupportPage, icon: 'help' }, */
    { title: 'Signup', name: 'SignupPage', component: SignupPage, icon: 'person-add' }
  ];
  rootPage: any;

  constructor(
    public events: Events,
    public userData: UserData,
    public menu: MenuController,
    public platform: Platform,
    public confData: ConferenceData,
    public storage: Storage,
    public splashScreen: SplashScreen,
    public oneSignal: OneSignal,
    private statusBar: StatusBar,
    public keyboard: Keyboard
  ) {

    // Check if the user has already seen the tutorial
    this.storage.get('hasSeenTutorial')
      .then((hasSeenTutorial) => {
        console.log(hasSeenTutorial);
        if (hasSeenTutorial) {
          this.storage.get('hasLoggedIn').then((value) => {
            if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
              this.oneSignal.startInit("80750dd5-6c91-4f08-944e-36f9044cf494", "885900586460");
              //this.oneSignal.iOSSettings({kOSSettingsKeyAutoPrompt:true, kOSSettingsKeyInAppLaunchURL: false});
              this.oneSignal.endInit();
            }
            if (value) {
              this.rootPage = TabsPage;
            } else {
              this.rootPage = LoginPage;
            }
          });
        } else {
          this.rootPage = TutorialPage;
        }
        this.platformReady()
      });


    // decide which menu items should be hidden by current login status stored in local storage
    this.userData.hasLoggedIn().then((hasLoggedIn) => {
      this.enableMenu(hasLoggedIn === true);
    });
    this.enableMenu(true);

    this.listenToLoginEvents();
  }

  openPage(page: PageInterface) {
    let params = {};

    // the nav component was found using @ViewChild(Nav)
    // setRoot on the nav to remove previous pages and only have this page
    // we wouldn't want the back button to show in this scenario
    if (page.index) {
      params = { tabIndex: params };
    }
    console.log('params', params);
    // If we are already on tabs just change the selected tab
    // don't setRoot again, this maintains the history stack of the
    // tabs even if changing them from the menu
    if (this.nav.getActiveChildNavs().length && page.index != undefined) {
      this.nav.getActiveChildNavs()[0].select(page.index);
    } else {
      // Set the root of the nav with params if it's a tab index
      this.nav.setRoot(page.component, params).catch((err: any) => {
        console.log(`Didn't set nav root: ${err}`);
      });
    }

    if (page.logsOut === true) {
      // Give the menu time to close before changing to logged out
      this.userData.logout();
      this.nav.setRoot(LoginPage);
    }
  }

  openTutorial() {
    this.nav.setRoot(TutorialPage);
  }

  openAbout() {
    this.nav.setRoot(AboutPage);
  }

  listenToLoginEvents() {
    this.events.subscribe('user:login', () => {
      this.enableMenu(true);
    });

    this.events.subscribe('user:signup', () => {
      this.enableMenu(true);
    });

    this.events.subscribe('user:logout', () => {
      this.enableMenu(false);
    });
  }

  enableMenu(loggedIn: boolean) {
    this.menu.enable(loggedIn, 'loggedInMenu');
    this.menu.enable(!loggedIn, 'loggedOutMenu');
  }

  platformReady() {
    // Call any initial plugins when ready
    this.platform.ready().then(() => {
      this.splashScreen.hide();
      this.keyboard.disableScroll(false);
      this.statusBar.hide();
      var sqlite = new SQLite;
      var sqlConf: any;
      if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
        this.userData.watchNetwork();
        if (this.platform.is('ios')) {
          sqlConf = {
            name: 'maxis.db',
            location: 'default'
          };
        }
        else {
          sqlConf = {
            name: 'maxis.db',
            iosDatabaseLocation: 'Documents'
          };
        }
        sqlite.create(sqlConf)
          .then((db: SQLiteObject) => {
            //create table activity
            /* db.executeSql('CREATE TABLE IF NOT EXISTS activity ('+
                          'activity_id INTEGER PRIMARY KEY AUTOINCREMENT,'+ 
                          'activity_assigned_to_id INTEGER,'+ 
                          'activity_name TEXT,'+ 
                          'progress_status INTEGER,'+
                          'dueDays TEXT,'+
                          'activity_object TEXT,'+
                          'activity_sync TEXT'+
                        ')', {})
              .then(() => {}).catch(e => console.log("error creating database:"+JSON.stringify(e))); */


            //create table custom forms
            /* db.executeSql('DROP TABLE customForm', {})
              .then(() => {console.log('customForm table removed!')}).catch(e => console.log("error creating database:"+JSON.stringify(e))); */
            db.executeSql('CREATE TABLE IF NOT EXISTS customForm (' +
              'form__id INTEGER PRIMARY KEY AUTOINCREMENT,' +
              'form_id INTEGER,' +
              'form_module_key TEXT,' +
              'form_module_id INTEGER,' +
              'form_project_id TEXT,' +
              'form_activity_id TEXT,' +
              'form_title TEXT,' +
              'form_description TEXT,' +
              'form_deleted INTEGER DEFAULT 0,' +
              'form_custom_fields TEXT,' +
              'form_created_on TEXT,' +
              'form_modified_on TEXT,' +
              'form_created_by TEXT,' +
              'form_modified_by TEXT,' +
              'form_sync INTEGER DEFAULT 0' +
              ')', {})
              .then(() => { console.log('customForm table created successfully!') })
              .catch(e => console.log("error creating database:" + JSON.stringify(e)));

            db.executeSql('CREATE TABLE IF NOT EXISTS updateTbl (' +
              'update_id INTEGER PRIMARY KEY AUTOINCREMENT,' +
              'update_on TEXT' +
              ')', {})
              .then(() => { console.log('Update Table created successfully!') })
              .catch(e => console.log("UpdateTbl error creating database:" + JSON.stringify(e)));

            db.executeSql("SELECT * FROM updateTbl ORDER BY update_on DESC", [])
              .then((result) => {
                if (result.rows.length == 0) {
                  this.userData.lastUpdatedDate = 0;
                } else {
                  this.userData.lastUpdatedDate = result.rows.item(0).update_on;
                }
              })
              .catch(e => console.log('update error: ' + JSON.stringify(e)));

          })
          .catch(e => alert(JSON.stringify(e)));
      }

      //this.userData.truncateTable('customForm');
      //this.userData.truncateTable('updateTbl');
      /* Observable.interval(5000).subscribe(() => {
        console.log("____________________________");
        this.userData.updateLocal();
        console.log("____________________________");
      }); */
    });
  }

  isActive(page: PageInterface) {
    let childNav = this.nav.getActiveChildNavs()[0];

    // Tabs are a special case because they have their own navigation
    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }
      return;
    }

    if (this.nav.getActive() && this.nav.getActive().name === page.name) {
      return 'primary';
    }
    return;
  }
}
