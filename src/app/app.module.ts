import 'intl';
import 'intl/locale-data/jsonp/en';

import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Keyboard } from '@ionic-native/keyboard';
import { OneSignal } from '@ionic-native/onesignal';
import { Geolocation } from '@ionic-native/geolocation';

import { IonicStorageModule } from '@ionic/storage';

import { ConferenceApp } from './app.component';
import { DatePipe } from '@angular/common';

import { TabsPage } from '../pages/tabs-page/tabs-page';
import { ProjectsPage } from '../pages/projects/projects';
import { LoginPage } from '../pages/login/login';
import { SiteInstructionPage } from '../pages/site-instruction/site-instruction';
import { SiteInstructionDetailPage } from '../pages/site-instruction-detail/site-instruction-detail';
import { SiteInstructionAddNodePage } from '../pages/site-instruction-add-node/site-instruction-add-node';
import { SiteInstructionCreatePage } from '../pages/site-instruction-create/site-instruction-create';
import { AboutPage } from '../pages/about/about';
import { PopoverPage } from '../pages/about-popover/about-popover';
import { SignupPage } from '../pages/signup/signup';
import { AccountPage } from '../pages/account/account';
import { MapPage } from '../pages/map/map';
import { SchedulePage } from '../pages/schedule/schedule';
import { ScheduleFilterPage } from '../pages/schedule-filter/schedule-filter';
import { SessionDetailPage } from '../pages/session-detail/session-detail';
import { SpeakerDetailPage } from '../pages/speaker-detail/speaker-detail';
import { SpeakerListPage } from '../pages/speaker-list/speaker-list';
import { SupportPage } from '../pages/support/support';
import { MyTaskPage } from '../pages/my-task/my-task';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { ProjectDetailPage } from '../pages/project-detail/project-detail';
import { ManholeDetailPage } from '../pages/manhole-detail/manhole-detail';
import { ManholeUpdatePage } from '../pages/manhole-update/manhole-update';
import { UpdateFormPage } from '../pages/update-form/update-form';
import { UpdateViewPage } from '../pages/update-view/update-view';
import { ActivityFilterPage } from '../pages/activity-filter/activity-filter'
import { WorkflowPage } from '../pages/workflow/workflow';
import { EotPage } from '../pages/eot/eot';
import { EotCreatePage } from '../pages/eot-create/eot-create';
import { ProjectContentPage } from '../pages/project-content/project-content'
import { ProjectCreatePage } from '../pages/project-create/project-create'
import { ProjectUpdatePage } from '../pages/project-update/project-update'
import { TpUploadPage } from '../pages/tp-upload/tp-upload'
import { ActivitiesDetailPage } from '../pages/activities-detail/activities-detail'
import { EmojiProvider } from '../providers/emoji';
import { CalendarPage } from '../pages/calendar/calendar';
import { CreateTaskPage } from "../pages/create-task/create-task";
import { TaskAssigneePage } from '../pages/task-assignee/task-assignee';
import { ModalToastPage } from '../pages/modal-toast/modal-toast';
import { CalendarDetailPage } from '../pages/calendar-detail/calendar-detail';
import { IzeBotPage } from '../pages/ize-bot/ize-bot';
import { IzebotPopoverPage } from '../pages/izebot-popover/izebot-popover';
import { IzeBotShortCodePage } from '../pages/ize-bot-short-code/ize-bot-short-code'; 
import { DashboardPage } from '../pages/dashboard/dashboard';
import { ChartsModule } from 'ng2-charts';
import { DiscussionMediaPage } from '../pages/discussion-media/discussion-media';
import 'chart.piecelabel.js';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';

import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';
import { NgCalendarModule } from 'ionic2-calendar';
import { DatePicker } from '@ionic-native/date-picker';
import { SQLite } from '@ionic-native/sqlite';
import { Vibration } from '@ionic-native/vibration';
import { TouchID } from '@ionic-native/touch-id';
import { Network } from '@ionic-native/network';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';


import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { UploadfileProvider } from '../providers/uploadfile/uploadfile';
import { TakePhotoProvider } from '../providers/take-photo/take-photo';
const config: SocketIoConfig = { url: 'http://cloud.izeberg.net:8081', options: {} };

@NgModule({
  declarations: [
    ConferenceApp,
    TabsPage,
    ProjectsPage,
    LoginPage,
    AboutPage,
    PopoverPage,
    SignupPage,
    AccountPage,
    MapPage,
    SchedulePage,
    ScheduleFilterPage,
    SessionDetailPage,
    SpeakerDetailPage,
    SpeakerListPage,
    SupportPage,
    MyTaskPage,
    DashboardPage,
    TutorialPage,
    ProjectDetailPage,
    ManholeDetailPage,
    ManholeUpdatePage,
    UpdateFormPage,
    UpdateViewPage,
    ActivityFilterPage,
    SiteInstructionPage,
    SiteInstructionDetailPage,
    SiteInstructionAddNodePage,
    SiteInstructionCreatePage,
    WorkflowPage,
    EotPage,
    EotCreatePage,
    ProjectContentPage,
    ProjectCreatePage,
    ProjectUpdatePage,
    TpUploadPage,
    ActivitiesDetailPage,
    CalendarPage,
    CreateTaskPage,
    TaskAssigneePage,
    ModalToastPage,
    CalendarDetailPage,
    IzeBotPage,
    IzeBotShortCodePage,
    DiscussionMediaPage,
    IzebotPopoverPage
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    NgCalendarModule,
    HttpModule,
    ChartsModule,
    IonicModule.forRoot(ConferenceApp, {
      scrollPadding: false,
      scrollAssist: false,
      autoFocusAssist: false
    }, {
        links: [
          /* { component: TabsPage, name: 'TabsPage', segment: 'tabs-page' },
          { component: MyTaskPage, name: 'MyTaskPage', segment: 'myTask' },
          { component: ProjectsPage, name: 'Project', segment: 'project' },
          { component: ProjectDetailPage, name: 'ProjectDetail', segment: 'ProjectDetail' },
          { component: SchedulePage, name: 'Schedule', segment: 'schedule' },
          { component: SessionDetailPage, name: 'SessionDetail', segment: 'sessionDetail/:sessionId' },
          { component: ScheduleFilterPage, name: 'ScheduleFilter', segment: 'scheduleFilter' },
          { component: SpeakerListPage, name: 'SpeakerList', segment: 'speakerList' },
          { component: SpeakerDetailPage, name: 'SpeakerDetail', segment: 'speakerDetail/:speakerId' },
          { component: MapPage, name: 'Map', segment: 'map' },
          { component: AboutPage, name: 'About', segment: 'about' },
          { component: TutorialPage, name: 'Tutorial', segment: 'tutorial' },
          { component: SupportPage, name: 'SupportPage', segment: 'support' },
          { component: LoginPage, name: 'LoginPage', segment: 'login' },
          { component: AccountPage, name: 'AccountPage', segment: 'account' },
          { component: SignupPage, name: 'SignupPage', segment: 'signup' } */
        ]
      }),
    SocketIoModule.forRoot(config),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ConferenceApp,
    TabsPage,
    ProjectsPage,
    LoginPage,
    AboutPage,
    PopoverPage,
    SignupPage,
    AccountPage,
    MapPage,
    SchedulePage,
    ScheduleFilterPage,
    SessionDetailPage,
    SpeakerDetailPage,
    SpeakerListPage,
    SupportPage,
    MyTaskPage,
    TutorialPage,
    ProjectDetailPage,
    ManholeDetailPage,
    ManholeUpdatePage,
    UpdateFormPage,
    UpdateViewPage,
    ActivityFilterPage,
    SiteInstructionPage,
    SiteInstructionDetailPage,
    SiteInstructionAddNodePage,
    SiteInstructionCreatePage,
    WorkflowPage,
    EotPage,
    EotCreatePage,
    ProjectContentPage,
    ProjectCreatePage,
    ProjectUpdatePage,
    TpUploadPage,
    DashboardPage,
    ActivitiesDetailPage,
    CalendarPage,
    CreateTaskPage,
    TaskAssigneePage,
    ModalToastPage,
    CalendarDetailPage,
    IzeBotPage,
    IzeBotShortCodePage,
    DiscussionMediaPage,
    IzebotPopoverPage
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ConferenceData,
    UserData,
    InAppBrowser,
    SplashScreen,
    StatusBar,
    EmojiProvider,
    OneSignal,
    DatePicker,
    SQLite,
    Vibration,
    TouchID,
    Network,
    Keyboard,
    Camera,
    DatePipe,
    File,
    FileTransfer,
    Transfer,
    FilePath,
    UploadfileProvider,
    TakePhotoProvider,
    Geolocation,
    Validators,
    ScreenOrientation,
    PhotoViewer,
    UniqueDeviceID
  ]
})
export class AppModule { }
