import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RelativeTime } from './relative-time';

@NgModule({
  declarations: [
    RelativeTime,
  ],
  imports: [
    IonicPageModule.forChild(RelativeTime),
  ],
})
export class RelativeTimeModule {}
