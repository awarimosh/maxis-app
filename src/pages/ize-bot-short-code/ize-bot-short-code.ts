import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IzeBotPage } from '../ize-bot/ize-bot';

/**
 * Generated class for the IzeBotShortCodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ize-bot-short-code',
  templateUrl: 'ize-bot-short-code.html',
})
export class IzeBotShortCodePage {
  private izeBotPage: IzeBotPage;
  izeBotShortCode: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.izeBotPage = navParams.get('izeBot');
    //this.izeBotShortCode = navParams.get('izeBotShortCode');
  }

  applyCode(){
    console.log(this.izeBotShortCode);
    //console.log(this.projectDetail);
    this.izeBotPage.changeFilter(this.izeBotShortCode);
  }

}
