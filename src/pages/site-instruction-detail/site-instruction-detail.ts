import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ModalController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { UserData } from '../../providers/user-data';
import { SiteInstructionAddNodePage } from '../site-instruction-add-node/site-instruction-add-node';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@IonicPage()
@Component({
  selector: 'page-site-instruction-detail',
  templateUrl: 'site-instruction-detail.html',
})
export class SiteInstructionDetailPage {
  record: any;
  userInfo: any;
  formNodes: any;
  resetNodes: any;
  formSubmitted: any;
  constructor(
              public navCtrl: NavController, 
              public ldrCtrl: LoadingController, 
              public actShetCtrl: ActionSheetController, 
              public modalCtrl: ModalController, 
              public navParams: NavParams,
              public http: Http,
              public storage: Storage, 
              public userData: UserData 
            ) {
    this.record = navParams.get('record');
    console.log(this.record);
  }

  ionViewDidLoad() {
    this.storage.get('userInfo').then((value) => {
      if(value){
        this.userInfo = value;
        this.getNodes(this.record.site_instruction_form_id); 
      }      
    }); 
  }

  getNodes(formId: any){
    //site_instruction_form/21?_getSiteFormNodesData=1
    var body = 'AuthKey='+this.userInfo.user_key+'&_getSiteFormNodesData=1';
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

  
    this.http.get(this.userData.base_url+'api/site_instruction_form/'+formId+'?'+body,{
          headers: headers
        })
        .map(res => res.json())
        .subscribe(data => {
            this.formSubmitted = data.response.data.form_submited;
            this.formNodes = data.response.data.nodes;
            this.resetNodes = data.response.data.nodes;
            console.log(this.formNodes);
        }, error => {
            console.log(JSON.stringify(error.json()));
        });
  }

  getMethods(methods: any){
    //var tmpStr = this.stripslashes(methods);
    var tmpStr = methods.replace(/\\/g, '');
    //console.log(tmpStr);
    return JSON.parse(tmpStr);
  }

  presentActionSheet(index: any) {
    /* console.log(JSON.stringify(this.formNodes[index]));
    let tempArr = {
      "site_instruction_detail_id": "658",
      "site_instruction_detail_form_id": "0",
      "site_instruction_detail_tp_id": "13",
      "site_instruction_detail_title": "Newly added",
      "site_instruction_detail_code": null,
      "site_instruction_detail_type": "distance",
      "site_instruction_detail_order": "1",
      "site_instruction_detail_lat": null,
      "site_instruction_detail_lng": null,
      "site_instruction_detail_new": "0",
      "site_instruction_detail_description": null,
      "site_instruction_detail_dateOfIssue": "2018-03-16",
      "site_instruction_detail_effectiveDate": null,
      "site_instruction_detail_distance": null,
      "site_instruction_detail_distances": "[{\"method\":\"GV\",\"value\":\"6\"}]"
    };
    this.formNodes.splice(index, 0, tempArr); */

    let actionSheet = this.actShetCtrl.create({
      buttons: [
        {
          text: 'Pole',
          icon: 'arrow-round-up',
          cssClass: 'text-start',
          handler: () => {
            this.addNodeDetail('pole',"Pole",index);
          }
        },
        {
          text: 'Manhole',
          icon: 'ios-disc-outline',
          cssClass: 'text-start',
          handler: () => {
            this.addNodeDetail('mh','Manhole',index);
          }
        },
        {
          text: 'Distance',
          icon: 'resize',
          cssClass: 'text-start',
          handler: () => {
            this.addNodeDetail('distance',"Method",index);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
 
    actionSheet.present();
  }

  addNodeDetail(nodeType: any, typeName: any, index: number){
    let data: any= {};
    if(nodeType == 'distance'){
      data.nodeATitle = this.formNodes[index-1].site_instruction_detail_title;
      data.nodeBTitle = this.formNodes[index].site_instruction_detail_title;
    }else{
      data = {
        "site_instruction_detail_form_id": "",
        "site_instruction_detail_tp_id": "",
        "site_instruction_detail_title": "",
        "site_instruction_detail_code": null,
        "site_instruction_detail_type": nodeType,
        "site_instruction_detail_order": "",
        "site_instruction_detail_lat": null,
        "site_instruction_detail_lng": null,
        "site_instruction_detail_new": "1",
        "site_instruction_detail_description": null,
        "site_instruction_detail_dateOfIssue": "",
        "site_instruction_detail_effectiveDate": null,
        "site_instruction_detail_distance": null,
        "site_instruction_detail_distances": ""
      }
    }
    let updateForm = this.modalCtrl.create(
                                            SiteInstructionAddNodePage, 
                                            {
                                              nodeType: nodeType,
                                              typeName: typeName, 
                                              extra: JSON.stringify(data)}
                                          );
    updateForm.onDidDismiss(data => {
      if(typeof(data) != 'undefined'){
        this.formNodes.splice(index, 0, data);
      }
      //this.getCustomFormUpdate();
    });
    updateForm.present();
  }

  revert(){
    this.resetNodes.forEach(element => {
      this.formNodes.push(element);
    });
  }

  submit(){
    let submitArr = [];
    let index = 0;
    this.formNodes.forEach(element => {
      if(typeof(element.site_instruction_detail_id) == 'undefined'){
        element.site_instruction_detail_id = 0;
      }
      let data = {
                  "title": element.site_instruction_detail_title,
                  "title_s": element.site_instruction_detail_code,
                  "id": 'node_' + Math.floor(Date.now())+"_"+element.site_instruction_detail_id,
                  "wfs_id": element.site_instruction_detail_id,
                  "new_node": element.site_instruction_detail_new,
                  "actionToggle": false,
                  "initToggle": false,
                  "sequence": index,
                  "type": element.site_instruction_detail_type,
                  "sequence_code": element.site_instruction_detail_new,
                  "lat": element.site_instruction_detail_lat,
                  "lng": element.site_instruction_detail_lng,
                  "description": element.site_instruction_detail_description,
                  "dateOfIssue": element.site_instruction_detail_dateOfIssue,
                  "effectiveDate": element.site_instruction_detail_effectiveDate,
                  "selectDistance": {
                    "method": "",
                    "value": ""
                  },
                  "distances": element.site_instruction_detail_distances
                };
      submitArr.push(data);
      index++;
    });
    console.log(JSON.stringify(submitArr));
    

    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    var body = 'AuthKey='+this.userInfo.user_key+
                    '&data='+JSON.stringify(submitArr)+
                    '&form_id='+this.record.site_instruction_form_id+
                    '&_SubmitSiteFormNodesData=1'+
                    '&_IsApp=1';

    let loading = this.ldrCtrl.create({
                    content: 'Submitting Site Instruction Form...'
                  });
    loading.present();

    this.http.post(this.userData.base_url+'api/site_instruction_form',
      body, {
      headers: headers
      })
      .map(res => res.json())
      .subscribe(data => {
        loading.dismiss();
        if(!data.response.error){   
          console.log(data.response.data);
          this.navCtrl.pop();
        }
      }, error => {
        console.log(JSON.stringify(error.json()));
      });
  }
}
