import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { UserData } from '../../providers/user-data';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the WorkflowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-workflow',
  templateUrl: 'workflow.html',
})
export class WorkflowPage {
  module_id: any;
  record_id: any;
  workdflowDetail: any;
  userInfo: any;
  constructor(
              public navCtrl: NavController, 
              public navParams: NavParams,
              public ldrCtrl: LoadingController,
              public http: Http,
              public storage: Storage, 
              public userData: UserData ,
              public viewCtrl: ViewController
            ) {
    this.module_id = navParams.get('module_id');
    this.record_id = navParams.get('record_id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WorkflowPage');
    
    this.storage.get('userInfo').then((value) => {
      if(value){
        this.userInfo = value;
        this.getWorkflow();
      }      
    }); 
  }

  getWorkflow(){
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    var body = 'AuthKey='+this.userInfo.user_key+
                    '&module_id='+this.module_id+
                    '&record_id='+this.record_id+
                    '&_workflowRecordData=1';      

    let loading = this.ldrCtrl.create({
      content: 'Updateing workflow...'
    });
    loading.present();
    this.http.post(this.userData.base_url+'api/workflow',
      body, {
      headers: headers
      })
      .map(res => res.json())
      .subscribe(data => {
        loading.dismiss();
        if(!data.response.error){  
          this.workdflowDetail = data.response.data;
          console.log(this.workdflowDetail);
        }
      }, error => {
        console.log(JSON.stringify(error.json()));
      });
  }


  closeModal(){
    this.viewCtrl.dismiss();
  }

  // Check if i can approv workflow
  isItMe(userList: any){
    var approver = userList.filter(user => user.user_id == this.userInfo.user_id)[0];
    if(approver && approver.user_key == this.userInfo.user_key){
      return true;
    }else
      return false;
  }

  radioModel(){
    return 'input';
  }

  getName(id: any, type: any){
    if(type == 'radio'){
      return "actionInput"+id;
    }
  }

  approveMsg: any= '';
  approveType: any= '';

  approve(id: any){
    /* 
    message:"This is approved..."
    module_id:100
    record_id:"40"
    seq_id:"85"
    status:"Approved"
    status:"Rejected"
    _workflowRecordDataSave:1 */
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    var body = 'AuthKey='+this.userInfo.user_key+
                    '&message='+this.approveMsg+
                    '&module_id='+this.module_id+
                    '&record_id='+this.record_id+
                    '&seq_id='+id+
                    '&status='+this.approveType+
                    '&_workflowRecordDataSave=1';  
    

    let loading = this.ldrCtrl.create({
      content: 'Updateing workflow...'
    });
    loading.present();
    this.http.post(this.userData.base_url+'api/workflow',
        body, {
        headers: headers
        })
        .map(res => res.json())
        .subscribe(data => {
          loading.dismiss();
          if(!data.response.error){  
            this.getWorkflow();
          }
        }, error => {
          console.log(JSON.stringify(error.json()));
        });
  }

  discard(){
    //{"record_id":"40","module_id":100,"_workflowDiscard":1}
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    var body = 'AuthKey='+this.userInfo.user_key+
                    '&module_id='+this.module_id+
                    '&record_id='+this.record_id+
                    '&_workflowDiscard=1';  
    

    let loading = this.ldrCtrl.create({
      content: 'Updateing workflow...'
    });
    loading.present();
    this.http.post(this.userData.base_url+'api/workflow',
        body, {
        headers: headers
        })
        .map(res => res.json())
        .subscribe(data => {
          loading.dismiss();
          if(!data.response.error){  
            this.getWorkflow();
          }
        }, error => {
          console.log(JSON.stringify(error.json()));
        });
  }
}
