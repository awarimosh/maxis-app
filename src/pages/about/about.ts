import { Component } from '@angular/core';
import { IonicPage, NavController , NavParams ,PopoverController,ModalController  } from 'ionic-angular';
import { PopoverPage } from '../about-popover/about-popover';
import { UserData } from '../../providers/user-data';
/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  conferenceDate = '2047-05-17';
  username: string;
  userInfo: any;
  badgesKeys: any;
  constructor(
    public popoverCtrl: PopoverController,
    public nav: NavController,
    public navParams: NavParams,
    public userData: UserData,
    public modalCtrl: ModalController
  ) {

  }

  ionViewWillEnter() {
    this.getUserInfo();
    console.log('about page 1');
  }

  ionViewDidLoad() {
    console.log('about page 2');
  }
  
  doRefresh(refresher) {
    this.getUserInfo();
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  ngAfterViewInit() {
    this.getUsername();
    this.getUserInfo();
  }

  getUsername() {
    this.userData.getUsername().then((username) => {
      this.username = username;
    });
  }


  logout() {
    this.userData.logout();
    this.nav.setRoot('LoginPage');
  }

  getUserInfo() {
    this.userData.getUserInfo().then((userInfo) => {
      this.userInfo = userInfo;
      this.badgesKeys = Object.keys(this.userInfo.user_badges);
      console.log(this.badgesKeys);
    });
  }

  presentPopover(event: Event) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({ ev: event });
  }
}
