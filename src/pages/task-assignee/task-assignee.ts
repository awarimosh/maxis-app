import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TaskAssigneePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-task-assignee',
  templateUrl: 'task-assignee.html',
})
export class TaskAssigneePage {

  activity_assigned_to_id: any;
  systemUsers: any;
  this_activity: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.activity_assigned_to_id = navParams.get('activity_assigned_to_id');
    this.systemUsers = navParams.get('systemUsers');
    this.this_activity = navParams.get('this_activity');

    console.log(this.systemUsers);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskAssigneePage');
  }

  applyAssigned(){
    //console.log(this.projectDetail);
    this.this_activity.changeAssignee(this.activity_assigned_to_id);
  }

  getUserPic(user_id){
    if(user_id != null){
      let user =  this.systemUsers.filter(user => user.user_id === user_id)[0];
      if(typeof(user) != 'undefined'){
        return user.user_display_pic;
      }else{
        return 'http://cloud.izeberg.com/maxis_osp/themes/core/assets/images/default.jpeg';
      }
    }else{
      return 'http://cloud.izeberg.com/maxis_osp/themes/core/assets/images/default.jpeg';
    }
  }

}
