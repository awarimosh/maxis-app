import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ModalToastPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-toast',
  templateUrl: 'modal-toast.html',
})
export class ModalToastPage {
  rewardPoints:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.rewardPoints = navParams.get('rewardPoints');
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalToastPage');
    console.log(this.rewardPoints);
  }

  public dismiss() {
      this.viewCtrl.dismiss();
  }

}
