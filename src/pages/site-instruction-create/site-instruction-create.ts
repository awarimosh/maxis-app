import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { UserData } from '../../providers/user-data';
import 'rxjs/add/operator/map';

/* import moment from 'moment';
import 'moment/locale/pt-br'; */
/**
 * Generated class for the SiteInstructionCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-site-instruction-create',
  templateUrl: 'site-instruction-create.html',
})
export class SiteInstructionCreatePage {
  formKey: any="site_instruction_form";
  moduleAttributesList: any;
  moduleSettings: any;
  moduleData: any;
  userInfo: any;
  constructor(
              public navCtrl: NavController, 
              public navParams: NavParams,
              public userData: UserData,
              public storage: Storage,
              public viewCtrl: ViewController,
              public http: Http,
            ) {
    this.moduleAttributesList = this.userData.moduleSettings[this.formKey].module_settings.attributesList[0];    
    this.moduleSettings = this.userData.moduleSettings[this.formKey].module_settings;    
    this.moduleData = this.userData.moduleSettings[this.formKey].module_data;
    console.log(this.moduleAttributesList);
  }

  ionViewDidLoad() {
    this.storage.get('userInfo').then((value) => {
      if(value){
        this.userInfo = value;
      }      
    }); 
  }

  closeModal(){
    this.viewCtrl.dismiss();
  }

  createSIF(form: any){
    console.log(JSON.stringify(form));
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    var body = 'AuthKey='+this.userInfo.user_key+
                    '&site_instruction_form_title='+form.site_instruction_form_title+
                    '&site_instruction_form_description='+form.site_instruction_form_description+
                    '&custom_147='+form.custom_147;      

    this.http.post(this.userData.base_url+'api/site_instruction_form',
      body, {
      headers: headers
      })
      .map(res => res.json())
      .subscribe(data => {
        if(!data.response.error){   
          this.closeModal();
          //console.log(data.response.data);
        }
      }, error => {
        console.log(JSON.stringify(error.json()));
      });
  }
}
