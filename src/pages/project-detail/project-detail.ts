import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams, PopoverController } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';
import { Http, Headers } from '@angular/http';
import { ActivitiesDetailPage } from '../activities-detail/activities-detail';
import { ActivityFilterPage } from '../activity-filter/activity-filter';

import moment from 'moment';
import 'moment/locale/pt-br';
/**
 * Generated class for the ProjectDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: "project-detail-page"
})
@Component({
  selector: 'page-project-detail',
  templateUrl: 'project-detail.html',
})
export class ProjectDetailPage {
  projectDetail: any;
  project_id: any;
  projectActivities: any;
  userInfo: any;
  systemUsers: any;
  domain: string;
  actSegment = "tasks";
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public popCtrl: PopoverController,
    public http: Http,
    public userData: UserData,
    public storage: Storage) {
    this.project_id = navParams.get('project_id');
    storage.get('userInfo').then((value) => {
      this.userInfo = value;
    });
    storage.get('systemUsers').then((value) => {
      this.systemUsers = value;
    });
  }

  ionViewDidLoad() {
    this.storage.get('domain').then((value) => {
      this.domain = value;
      this.getProject();
      this.getActivities();
    });
  }

  getProject() {
    var body = 'AuthKey=' + this.userInfo.user_key + '&_getViewData=1';
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    //this.http.get('https://'+this.domain+'.izeberg.com/maxis_osp/api/projects/'+this.project_id+'?'+body,{
    this.http.get('http://localhost/api/projects/' + this.project_id + '?' + body, {
      headers: headers
    })
      .map(res => res.json())
      .subscribe(data => {
        this.projectDetail = data.response.data.record;
        console.log(this.projectDetail.project_tools_data.users.data);
      }, error => {
        console.log(JSON.stringify(error.json()));
      });
  }

  getActivities() {
    var body = 'AuthKey=' + this.userInfo.user_key + '&_getActivitiesByProject=1&project_id=' + this.project_id;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    //this.http.get('https://'+this.domain+'.izeberg.com/maxis_osp/api/activities/?'+body,{
    this.http.get('http://localhost/api/activities/?' + body, {
      headers: headers
    })
      .map(res => res.json())
      .subscribe(data => {
        if (data.response.data) {
          data.response.data.forEach(activity => {
            if (activity.progress) {
              activity.checked = (activity.progress[0].progress_status == 1) ? false : true;
            }
            activity.progress_status = activity.progress ? activity.progress[0].progress_status : false;
            if (activity.activity_end_datetime) {
              activity.endDate = moment(activity.activity_end_datetime, "YYYY-MM-DD HH:mm:ss").format("DD MMM, YYYY")
              activity.end = moment(activity.activity_end_datetime, "YYYY-MM-DD HH:mm:ss").format("YYYY-MM-DD")
            }
            if (activity.activity_start_datetime) {
              activity.startDate = moment(activity.activity_start_datetime, "YYYY-MM-DD HH:mm:ss").format("DD MMM, YYYY")
              activity.start = moment(activity.activity_start_datetime, "YYYY-MM-DD HH:mm:ss").format("YYYY-MM-DD")
            } else {
              activity.startDate = activity.endDate;
              activity.start = activity.end;
            }
          });
        }
        this.projectActivities = data.response.data;
      }, error => {
        console.log(JSON.stringify(error.json()));
      });
  }

  /* openDetail(activity: any){

    console.log(this.projectDetail);
    this.navCtrl.push(ActivitiesDetailPage,{activity: activity, project_id: this.project_id, projectDetail: this.projectDetail});
  } */

  openDetail(activity: any, status: any) {
    this.navCtrl.push(ActivitiesDetailPage, {
      activity: activity,
      status: status,
      project_id: activity.activity_project_id,
      myActivities: this.projectActivities,
      callBack: this.updateStatus
    });

  }
  updateStatus(myActivities: any, returnActivity: any, data: any) {
    this.projectActivities = myActivities;
    this.projectActivities.forEach(element => {
      if (element.activity_id === returnActivity) {
        element.progress_status = data;
      }
    });
  }

  getUserPic(user_id) {
    if (user_id != null) {
      let user = this.systemUsers.filter(user => user.user_id === user_id)[0];
      if (typeof (user) != 'undefined') {
        return user.user_image_link;
      } else {
        return 'http://cloud.izeberg.com/maxis_osp/themes/core/assets/images/default.jpeg';
      }
    } else {
      return 'http://cloud.izeberg.com/maxis_osp/themes/core/assets/images/default.jpeg';
    }
  }

  getAssignedName(user_id) {
    let user = this.systemUsers.filter(user => user.user_id === user_id)[0];
    if (typeof (user) != 'undefined') {
      return user.user_name;
    } else {
      return 'Yet to assign';
    }
  }

  segmentChanged(event: any) {
    console.log(event._value);
  }

  today: any;
  m: any;
  getActivityDueDateCheck(item) {
    var check = '0';
    if (item.endDate) {
      this.m = moment(item.endDate, "DD MMM, YYYY"); // or whatever start date you have
      this.today = moment().startOf('day');
      var days = Math.round((this.today - this.m) / 86400000);
      if (days == 0) {
        check = 'priority-high';
      } else if (days > 0 && item.checked === false) {
        check = 'priority-urgent';
      } else if (days > 0 && item.checked === true) {
        check = 'priority-ok';
      } else if (days < 0) {
        check = 'priority-ok';
      } else if (days < 0) {
        check = '';
      }
    }
    return check;
  }

  activityFilter = 1;
  openFilter(ev: any) {
    let popover = this.popCtrl.create(ActivityFilterPage,
      { activityFilter: this.activityFilter, projectDetail: this });
    popover.onDidDismiss(value => {
      if (value) {
        this.activityFilter = value;
      }
    });
    popover.present({
      ev: ev
    });
  }

  changeFilter(filterVal: any) {
    this.activityFilter = filterVal;
  }
}
