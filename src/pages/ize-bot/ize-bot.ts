import {Component, ViewChild, NgZone} from '@angular/core';
import {IonicPage, NavController, Platform, NavParams, Content, PopoverController} from 'ionic-angular';
import {Keyboard} from '@ionic-native/keyboard';
import {Storage} from '@ionic/storage';
import {IzeBotShortCodePage} from '../ize-bot-short-code/ize-bot-short-code';
import {Http, Headers} from '@angular/http';
import {SQLite, SQLiteObject} from '@ionic-native/sqlite';

// import { ActivitiesDetailPage } from '../activities-detail/activities-detail';
// import {ProjectDetailPage} from '../project-detail/project-detail';
import {UserData} from '../../providers/user-data';

import {ManholeDetailPage} from '../manhole-detail/manhole-detail'

/**
 * Generated class for the IzeBotPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

import moment from 'moment';
import 'moment/locale/pt-br';

declare var ApiAIPromises: any;

@IonicPage()
@Component({
  selector: 'page-ize-bot',
  templateUrl: 'ize-bot.html',
  providers: [
    Keyboard,
    Content
  ]
})

export class IzeBotPage {
  @ViewChild(Content) content: Content;
  @ViewChild('chat_input') chatInput;
  tabBarElement: any;
  izeBotMsg: any;
  discussionMsg: any;
  question = '';
  answer: any;
  discussions: any;
  userInfo: any;
  domain: any;
  systemUsers: any;
  msgLoading = false;
  botFilter = 'myTask';
  public recording = false;
  sqlConf: any;

  private accessToken = '07a96d71bbc54419b43228ca613446e5' //best to take from environmental variables

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public platform: Platform,
              public ngZone: NgZone,
              public storage: Storage,
              public popCtrl: PopoverController,
              public http: Http,
              public keyboard: Keyboard,
              public userData: UserData,
              public sqlite: SQLite) {
    if (this.platform.is('ios')) {
      this.sqlConf = {
        name: 'izeberg.db',
        location: 'default'
      };
    }
    else {
      this.sqlConf = {
        name: 'izeberg.db',
        iosDatabaseLocation: 'Documents'
      };
    }
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    storage.get('userInfo').then((value) => {
      this.userInfo = value;
      if (this.userInfo != null) {
        this.storage.get('domain').then((value) => {
          this.domain = value;
          storage.get('systemUsers').then((value) => {
            this.systemUsers = value;
          })
            .catch(() => console.log('error loading activities'));
        });
      } else {
        this.navCtrl.setRoot("LoginPage");
      }
    });

    platform.ready().then(() => {
      if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
        ApiAIPromises.init({
          clientAccessToken: this.accessToken,
          lang: "en"
        }).then((data) => {
          console.log('ApiAi initialised', data);
        }).catch((error) => {
          console.error('ApiAi Error: ', error)
        });
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IzeBotPage');
    this.platform.ready().then(() => {
      this.keyboard.disableScroll(false);
    });
  }

  /* ionViewWillEnter() {
    if(this.tabBarElement != null){
      this.tabBarElement.style.display = 'none';
      this.content.resize();
    }
  }
  ionViewWillLeave(){
    if(this.tabBarElement != null){
      this.tabBarElement.style.display = 'flex';
    }
  } */

  sendMsg() {
    this.izeBotMsg = this.discussionMsg;
  }

  ask() {
    if (typeof (this.discussions) != 'undefined') {
      this.discussions.push({
        user_id: this.userInfo.user_id,
        user_display_pic: this.userInfo.user_display_pic,
        user_name: this.userInfo.user_name,
        discussion_created_on: "2017-12-07 04:40:00",
        discussion_comments: this.question,
        discussion_type: 'ques',
        myActivities: []
      });
    } else {
      this.discussions = [];
      this.discussions[0] = {
        user_id: this.userInfo.user_id,
        user_display_pic: this.userInfo.user_display_pic,
        user_name: this.userInfo.user_name,
        discussion_created_on: "2017-12-07 04:40:00",
        discussion_comments: this.question,
        myActivities: []
      };
    }
    this.msgLoading = true;
    var apiQues = this.question;
    this.question = '';
    this.content.resize();
    this.scrollToBottom();
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      try {
        ApiAIPromises.requestText({
          query: apiQues
        })
          .then(({result}) => {
            this.ngZone.run(() => {
              this.query(result.action, result.parameters);
              this.question = '';
              this.scrollToBottom();
            });
          });
      }
      catch (exp) {
        console.log("exp", exp.toString());
        this.userData.presentToast("ex " + exp.toString());
      }
    }
  }

  sendVoice() {
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.recording = true;
      try {
        // !!!
        ApiAIPromises.levelMeterCallback(function (level) {
          console.log(level);
        });

        //ApiAIPromises.setListeningStartCallback(this.recorderStart);

        //ApiAIPromises.setListeningFinishCallback(this.recorderStop);

        ApiAIPromises.requestVoice({})
          .then(({result}) => {
            this.ngZone.run(() => {
              this.answer = result.action;
              this.question = result.resolvedQuery;
              this.recording = false;
              if (typeof (this.discussions) != 'undefined') {
                this.discussions.push({
                  user_id: this.userInfo.user_id,
                  user_display_pic: this.userInfo.user_display_pic,
                  user_name: this.userInfo.user_name,
                  discussion_created_on: "2017-12-07 04:40:00",
                  discussion_comments: this.question,
                  discussion_type: 'ques',
                  myActivities: []
                });
              } else {
                this.discussions = [];
                this.discussions[0] = {
                  user_id: this.userInfo.user_id,
                  user_display_pic: this.userInfo.user_display_pic,
                  user_name: this.userInfo.user_name,
                  discussion_created_on: "2017-12-07 04:40:00",
                  discussion_comments: this.question,
                  myActivities: []
                };
              }
              this.msgLoading = true;
              this.question = '';
              this.content.resize();
              this.scrollToBottom();
              this.query(this.answer, result.parameters);
            });
          });
      } catch (e) {
        alert(e);
      }
    }
  }

  /* inArray(arr: any, s: string){
    arr.find();
  } */

  query(speech: any, params: any) {
    var body = '',title = 'Projects List';
    var headers = new Headers();
    if (speech == 'projectHealth') {
      body = 'AuthKey=' + this.userInfo.user_key + '&_getIonicProjectHealth=1';
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      this.http
        .get('https://' + this.domain + '.izeberg.com/maxis_osp/api/projects?' + body, {
          headers: headers
        })
        .map(res => res.json())
        .subscribe(data => {
          var projectList = data.response.data;
          if (data.response.data) {
            this.msgLoading = false;
            this.discussions.push({
              user_id: 0,
              user_display_pic: 'assets/img/izeberg-logo-pyramid-100.png',
              user_name: this.userInfo.user_name,
              discussion_created_on: "2017-12-07 04:40:00",
              discussion_comments: '',
              discussion_type: 'projectFilter',
              myActivities: projectList
            });
            this.scrollToBottom();
          }
        }, error => {
          console.log(JSON.stringify(error.json()));
        });
    } else if (speech == 'tag') {
      this.sqlite.create(this.sqlConf)
        .then((db: SQLiteObject) => {
          //data insert section
          db.executeSql("SELECT * FROM activity", [])
            .then((result) => {
              if (result.rows.length > 0) {
                var tag_detail = params.tag_detail.toLowerCase();
                var taskList = [];
                var dummyActivity = JSON.parse(result.rows.item(0).activity_object);
                for (var i = 0; i < result.rows.length; i++) {
                  var activity = JSON.parse(result.rows.item(i).activity_object);
                  var tags = JSON.parse(activity.activity_tags);
                  tags = tags.map(function (value) {
                    return value.toLowerCase();
                  });
                  console.log(JSON.stringify(tags));
                  if (tags.indexOf(tag_detail) != '-1') {
                    var tempAct = JSON.parse(result.rows.item(i).activity_object);
                    tempAct.query = "#" + tag_detail;
                    taskList.push(tempAct);
                  }
                }
                this.msgLoading = false;
                if (taskList.length > 0) {
                  this.discussions.push({
                    user_id: 0,
                    user_display_pic: 'assets/img/izeberg-logo-pyramid-100.png',
                    user_name: this.userInfo.user_name,
                    discussion_created_on: "2017-12-07 04:40:00",
                    discussion_comments: '',
                    discussion_type: 'taskFilter',
                    myActivities: taskList
                  });
                } else {
                  dummyActivity.query = "#" + tag_detail;
                  dummyActivity.activity_name = "No activity matched!";
                  dummyActivity.activity_dummy = true;
                  taskList.push(dummyActivity);
                  this.discussions.push({
                    user_id: 0,
                    user_display_pic: 'assets/img/izeberg-logo-pyramid-100.png',
                    user_name: this.userInfo.user_name,
                    discussion_created_on: "2017-12-07 04:40:00",
                    discussion_comments: '',
                    discussion_type: 'taskFilter',
                    myActivities: taskList
                  });
                }

                this.scrollToBottom();
              }
            })
            .catch(e => console.log(e));
        })
        .catch(e => alert(JSON.stringify(e)));
    } else {

      // body = 'AuthKey=' + this.userInfo.user_key + '&_getIonicProjectHealth=1';
      body = 'AuthKey=' + this.userInfo.user_key;

      if (speech == 'completedTask') {
        // alert("complete : " + speech + " p " + JSON.stringify(params));
        body = body + '&_getProjectStatus=0';
        title = "Projects Completed"
      }
      else if (speech == 'overdueTask') {
        // alert("overdue : " + speech + " p " + JSON.stringify(params));
        body = body + '&_getProjectStatus=1';
        title = "Projects behind the schedule";
      }
      else {
        this.userData.presentToast(speech);
      }
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      this.http
        .get('https://' + this.domain + '.izeberg.com/maxis_osp/api/projects?' + body, {
          headers: headers
        })
        .map(res => res.json())
        .subscribe(data => {
          var projectList = data.response.data;
          if (data.response.data) {
            this.msgLoading = false;
            this.discussions.push({
              user_id: 0,
              user_display_pic: 'assets/img/izeberg-logo-pyramid-100.png',
              user_name: this.userInfo.user_name,
              discussion_created_on: "2017-12-07 04:40:00",
              discussion_comments: '',
              discussion_type: 'projectFilter',
              myActivities: projectList,
              discussion_title : title
            });
            this.scrollToBottom();
          }
        }, error => {
          console.log(JSON.stringify(error.json()));
          alert(JSON.stringify(error.json()));
        });
    }
  }

  openDetail(activity: any, status: any) {
    console.log(status);
    // this.navCtrl.push(ActivitiesDetailPage, {
    //   activity: activity,
    //   status: status,
    //   project_id: activity.activity_project_id,
    //   myActivities: [],
    //   callBack: this.updateStatus
    // });
    this.navCtrl.push(ManholeDetailPage, {
      project_id: activity.activity_project_id,
      project_name: activity.activity_project_name
    });
  }

  updateStatus(myActivities: any, returnActivity: any, data: any) {
    myActivities = myActivities;
    returnActivity = returnActivity;
    data = data;
    /* this.myActivities = myActivities;
    if(this.myActivities.length > 0){
      this.myActivities.forEach(element => {
          if(element.activity_id === returnActivity){
            element.progress_status = data;
          }
      });
    } */
  }

  today: any;
  m: any;

  getActivityDueDateStatus(item) {
    if (item.end) {
      this.m = moment(item.end, "YYYY-MM-DD"); // or whatever start date you have
      this.today = moment().startOf('day');
      var days = this.m.diff(this.today, 'days');
      if (days == 0) {
        return "todayTask";
      } else if (days > 0 && days < 7 && item.checked === false) {
        return "endingSoon";
      } else if (days < 0 && item.checked === false) {
        return "overdueTask";
      } else if (days < 0 && item.checked === true) {
        return "completedTask";
      } else {
        return days;
      }
    }

  }

  izeBotShortCode = 1;
  shortCodePop: any;

  getshortCode(ev: any) {
    this.shortCodePop = this.popCtrl.create(IzeBotShortCodePage,
      {
        izeBotShortCode: this.izeBotShortCode,
        izeBot: this
      },
      {cssClass: "minWidth"}
    );
    this.shortCodePop.onDidDismiss(value => {
      if (value) {
        this.izeBotShortCode = value;
      }
    });
    this.shortCodePop.present({
      ev: ev
    });
  }

  changeFilter(filterVal: any) {
    this.question = filterVal;
    this.shortCodePop.dismiss();
    this.ask();
  }

  loadProject(project_id, project_name) {
    this.navCtrl.push(ManholeDetailPage, {
      project_id: project_id,
      project_name: project_name
    });
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content._scroll) {
        this.content.scrollToBottom();
      }
    }, 400)
  }
}
