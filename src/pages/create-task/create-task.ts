import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';

/**
 * Generated class for the CreateTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-task',
  templateUrl: 'create-task.html',
  providers: [
		Keyboard,
		Content
	]
})
export class CreateTaskPage {
  @ViewChild(Content) content: Content;
  tabBarElement: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  }

  ionViewWillEnter() {
    if(this.tabBarElement != null){
      this.tabBarElement.style.display = 'none';
      this.content.resize();
    }
  }
  ionViewWillLeave(){
    if(this.tabBarElement != null){
      this.tabBarElement.style.display = 'flex';
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateTaskPage');
  }

}
