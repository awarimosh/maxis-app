import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { UserData } from '../../providers/user-data';
import 'rxjs/add/operator/map';
/**
 * Generated class for the ProjectCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-project-create',
  templateUrl: 'project-create.html',
})
export class ProjectCreatePage {
  formKey: any="projects";
  moduleAttributesList: any;
  moduleSettings: any;
  moduleData: any;
  userInfo: any;
  constructor(
              public navCtrl: NavController, 
              public navParams: NavParams,
              public userData: UserData,
              public storage: Storage,
              public viewCtrl: ViewController,
              public http: Http,
            ) {
      this.moduleAttributesList = this.userData.moduleSettings[this.formKey].module_settings.attributesList[0];    
      this.moduleSettings = this.userData.moduleSettings[this.formKey].module_settings;    
      this.moduleData = this.userData.moduleSettings[this.formKey].module_data;
      console.log(this.moduleAttributesList);
  }

  ionViewDidLoad() {
    this.storage.get('userInfo').then((value) => {
      if(value){
        this.userInfo = value;
      }      
    });
  }

  closeModal(){
    this.viewCtrl.dismiss();
  }

  createProject(form: any){
    console.log(JSON.stringify(form));
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    var body = 'AuthKey='+this.userInfo.user_key+
                    '&project_data='+JSON.stringify(form)+
                    '&_project_create_app=1';

    this.http.post(this.userData.base_url+'api/projects',
      body, {
      headers: headers
      })
      .map(res => res.json())
      .subscribe(data => {
        if(!data.response.error){   
          this.closeModal();
          //console.log(data.response.data);
        }
      }, error => {
        console.log(JSON.stringify(error.json()));
      });
  }

}
