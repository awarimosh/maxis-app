import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, ToastController, Platform, NavParams, Refresher, LoadingController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { OneSignal } from '@ionic-native/onesignal';

import { ManholeDetailPage } from '../manhole-detail/manhole-detail'
//import { ProjectDetailPage } from '../project-detail/project-detail'
import { ProjectContentPage } from '../project-content/project-content'
import { ProjectCreatePage } from '../project-create/project-create';
import { TpUploadPage } from '../tp-upload/tp-upload';
import { UserData } from '../../providers/user-data';
import { UploadfileProvider } from '../../providers/uploadfile/uploadfile';
import { TakePhotoProvider } from '../../providers/take-photo/take-photo';
import { File } from '@ionic-native/file';
/* import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path'; */
import { Camera } from '@ionic-native/camera';
import { UpdateFormPage } from '../update-form/update-form';
// import { ProjectUpdatePage } from '../project-update/project-update';
import { SQLite } from '@ionic-native/sqlite';
import { ScreenOrientation } from '@ionic-native/screen-orientation';


import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import moment from 'moment';
import 'moment/locale/pt-br';


//declare var ApiAIPromises: any;

@IonicPage()
@Component({
  selector: 'page-projects',
  templateUrl: 'projects.html',
})
export class ProjectsPage {
  projects: any;
  activity: any;
  userInfo: any;
  domain: string;
  question = "";
  answer = "";
  izeBot = false;
  lastImage: string = null;
  image: any = [
    { "name": "Image 1", "model": 'image 1' },
  ];
  constructor(
    public userData: UserData,
    public http: Http,
    public navCtrl: NavController,
    public storage: Storage,
    public oneSignal: OneSignal,
    public platform: Platform,
    public ngZone: NgZone,
    public navParams: NavParams,
    public loading: LoadingController,
    public capturePhoto: TakePhotoProvider,
    public tstCtrl: ToastController,
    public uploadFile: UploadfileProvider,
    public file: File,
    public camera: Camera,
    public modalCtrl: ModalController,
    public sqlite: SQLite,
    public screenOrientation: ScreenOrientation
  ) {
    this.activity = navParams.get('activity');
  }

  syncUpdateForm() {
    this.userData.syncCustomForm();
  }

  updateLocal() {
    this.userData.updateLocal();
  }

  takePhoto(key: any) {
    // Create options for the Camera Dialog
    var options = {
      quality: 40,
      sourceType: this.camera.PictureSourceType.CAMERA,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library

      var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
      var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);

      var name = this.capturePhoto.createFileName(this.image[key].name.replace(/\s+/g, '-').toLowerCase());
      this.capturePhoto.copyFileToLocalDir(correctPath, currentName, name);
      //this.image[key].model = name;
    }, (err) => {
      this.userData.presentToast(err);
    });
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return this.file.dataDirectory + img;
    }
  }

  public async uploadAll() {
    const promisesArray: any[] = [];

    this.image.forEach(v => {
      console.log("==============Um uploading.. ");
      console.log(JSON.stringify(v));
      console.log("==============Um uploading.. ");
      if (v.model) {
        promisesArray.push(this.uploadFile.uploadImage(v.name, v.model));
      }
    });

    await Promise.all(promisesArray)
      .then((res) => {
        console.log("All uploads done" + res);
      }, (firstErr) => {
        console.error("Error uploading file.", firstErr);
      });
  }

  ionViewDidLoad() {
    this.storage.get('userEmail').then((userEmail) => {
      if (userEmail) {
        if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
          this.oneSignal.startInit("80750dd5-6c91-4f08-944e-36f9044cf494", "885900586460");
          //this.oneSignal.iOSSettings({kOSSettingsKeyAutoPrompt:true, kOSSettingsKeyInAppLaunchURL: false});
          this.oneSignal.sendTags({ userEmail: userEmail });
          this.oneSignal.endInit();
        }
      }
    });
    this.storage.get('userInfo').then((value) => {
      if (value) {
        this.userInfo = value;
        this.storage.get('domain').then((value) => {
          this.domain = value;
          this.getProjects();
        });
      }
    });
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      this.screenOrientation.unlock();
    }
  }

  createProject() {
    console.log('creating project');
    let projectCreate = this.modalCtrl.create(ProjectCreatePage);

    projectCreate.onDidDismiss(data => {
      data = data;
      this.getProjects();
    });
    projectCreate.present();
  }

  getProjects() {
    var body = 'AuthKey=' + this.userInfo.user_key + '&_getIonicProject=1';
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    let loading = this.loading.create({
      content: 'Loading Projects...'
    });

    loading.present();

    this.http
      .get(this.userData.base_url + 'api/projects?' + body, {
        headers: headers
      })
      .map(res => res.json())
      .subscribe(data => {
        this.projects = data.response.data;
        // var url = 'data:text/json;charset=utf8,' + encodeURIComponent(JSON.stringify(this.projects));
        // console.log('projects',url);
        data.response.data.forEach(projects => {
          if (projects.project_start_date) {
            projects.project_start_date = moment(projects.project_start_date, "YYYY-MM-DD").format("DD MMM, YYYY");
            projects.project_end_date = moment(projects.project_end_date, "YYYY-MM-DD").format("DD MMM, YYYY");
          }
        });
        this.userData.saveToStorage('projects', JSON.stringify(this.projects));
        loading.dismiss();
      }, error => {
        console.log('error', JSON.stringify(error.json()));
      });
    this.userData.getCustomForms();
  }

  async getProjectsOffline() {
    let loading = this.loading.create({
      content: 'Loading Offline Projects...'
    });

    loading.present();
    let projs = await this.userData.getFromStorage('projects');
    if (projs != undefined || null) {
      this.projects = JSON.parse(projs);
      this.projects.forEach(projects => {
        if (projects.project_start_date) {
          projects.project_start_date = moment(projects.project_start_date, "YYYY-MM-DD").format("DD MMM, YYYY");
          projects.project_end_date = moment(projects.project_end_date, "YYYY-MM-DD").format("DD MMM, YYYY");
        }
      });
    }
    else {
      this.userData.presentToast("offline data unavailable, please connect to the internet");
    }
    loading.dismiss();
    this.userData.getCustomFormsOffline();
  }

  doRefreshProject(refresher: Refresher) {
    refresher.complete();
  }

  loadProject(project_id, project_name) {
    this.navCtrl.push(ManholeDetailPage, {
      project_id: project_id,
      project_name: project_name
    });
  }

  showProjectDetail(project: any) {
    this.navCtrl.push(ProjectContentPage, {
      project: project
    });
  }

  uploadTp(project_id: any) {
    let uploadTp = this.modalCtrl.create(TpUploadPage, { project_id: project_id });

    uploadTp.onDidDismiss(data => {
      data = data;
      this.getProjects();
    });
    uploadTp.present();
  }

  askIzeBot() {
    if (!this.izeBot) {
      this.izeBot = true;
    } else {
      this.izeBot = false;
    }
  }

  /* showE2E(project: any) {
    let projectUpload = this.modalCtrl.create(ProjectUpdatePage, {project : project});

    projectUpload.onDidDismiss(data => {
      data = data;      
      this.getProjects();
    });
    projectUpload.present();
  } */

  showE2E(project: any) {
    if (this.activity == undefined || this.activity == null) {
      this.activity = {
        activity_project_id: project.project_id,
        activity_id: null
      }
    }
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      let updateForm = this.modalCtrl.create(
        UpdateFormPage,
        { formKey: 'e2e', name: 'E2E', activity: this.activity }
      );
      updateForm.onDidDismiss(() => {
        this.ionViewDidLoad();
      });
      updateForm.present();
    }
    else {
      console.log('only available on mobile')
      console.log('form', JSON.stringify(this.activity));
    }
  }

  /* ask() {
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      ApiAIPromises.requestText({
        query: this.question
      })
      .then(({result: {fulfillment: {speech}}}) => {
        this.ngZone.run(()=> {
          this.answer = speech;
          alert(this.answer);
        });
      });
    }
  } */
}
