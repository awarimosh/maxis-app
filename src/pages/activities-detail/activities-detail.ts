import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, NavController, ViewController, ToastController, ModalController, NavParams, Content, TextInput, PopoverController, ActionSheetController } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
//import { ChatService, ChatMessage, UserInfo } from "../../providers/chat-service";
import { Storage } from '@ionic/storage';
import { UserData } from '../../providers/user-data';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Socket } from 'ng-socket-io';
import { DatePicker } from '@ionic-native/date-picker';
import { ModalToastPage } from '../modal-toast/modal-toast';
import { Vibration } from '@ionic-native/vibration';
import {Camera, CameraOptions} from '@ionic-native/camera';
//import { File } from '@ionic-native/file';
//import { FileTransfer } from '@ionic-native/file-transfer';


import { TaskAssigneePage } from '../task-assignee/task-assignee';

import moment from 'moment';
import 'moment/locale/pt-br';

@IonicPage()
@Component({
  selector: 'page-activities-detail',
  templateUrl: 'activities-detail.html',
	providers: [
		Keyboard,
		Content
	]
})
export class ActivitiesDetailPage {
  @ViewChild(Content) content: Content;
  @ViewChild('chat_input') messageInput: TextInput;
  activity: any;
  project_id: any;
  projectDetail: any;
  domain: string;
  userInfo: any;
  systemUsers: any;
  progress= 60;
  discussions: any;
  editorMsg = '';
  showEmojiPicker = false;
  tabBarElement: any;
  discussionMsg= '';
  socketHost: any;
  cHeight: any;
  isComplete= false;
  activityStatus: any;
  callBack: any;
  myActivities: any;
  startDate: string;
  endDate: string;
  projects: any;
  activity_project_id: any;
  completeButtonDisabled = false;
  constructor(public navCtrl: NavController, 
              public viewCtrl: ViewController, 
              public toastCtrl: ToastController, 
              public navParams: NavParams,
              public storage: Storage,
              public http: Http, 
              public userData: UserData,
              public socket: Socket,
              public keyboard: Keyboard,
              public popCtrl: PopoverController,
              public datePicker: DatePicker,
              public modalCtrl: ModalController,
              public vibrate: Vibration,
              public platform: Platform,
              public actionSheet: ActionSheetController,
              public camera: Camera,
              //public file: File,
              //public fileTransfer: FileTransfer
            ) {

    this.activity = navParams.get('activity');
    this.activity.activity_tags_parsed = JSON.parse(this.activity.activity_tags);
    console.log(this.activity.activity_tags);
    this.activity_project_id = this.activity.activity_project_id;
    this.startDate = this.activity.start;
    this.endDate = this.activity.end;

    this.activityStatus = navParams.get('status');
    if(this.activityStatus == 2){
      this.isComplete = true;
    }

    this.callBack = this.navParams.get('callBack');
    this.myActivities = this.navParams.get('myActivities');

    this.projectDetail = navParams.get('projectDetail');
    this.project_id = navParams.get('project_id');
    storage.get('userInfo').then((value) => {
      this.userInfo = value;
      this.getProjects();
    });
    storage.get('systemUsers').then((value) => {
      this.systemUsers = value;
    });
    
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    
    /* if(this.content){
			this.cHeight = this.content.contentHeight;
		} else {
			this.cHeight = 0;
		} */

		/* this.keyboard.onKeyboardShow()
			.subscribe(data => {
        this.content.resize();
        this.scrollToBottom();
        data = data;
				this.cHeight = this.content.contentHeight;
			});
		this.keyboard.onKeyboardHide()
			.subscribe(data => {
        data = data;
				this.content.resize();
        this.scrollToBottom();
				this.cHeight = this.content.contentHeight;
			}); */
  }

  closeKeyboard($event){
		$event.preventDefault();
		if(document.activeElement){
			document.querySelector('input').blur();
		}
	}

  ionViewWillUnload(){
    this.callBack(this.myActivities, this.activity.activity_id, this.activityStatus);
  }

  ionViewWillEnter() {
    if(this.tabBarElement != null){
      this.tabBarElement.style.display = 'none';
      this.content.resize();
    }
  }

  ionViewDidEnter() {
    this.platform.ready().then(() => {
      this.keyboard.disableScroll(false);
    });
  }

  ionViewWillLeave(){
    if(this.tabBarElement != null){
      this.tabBarElement.style.display = 'flex';
    }
    /* this.platform.ready().then(() => {      
      this.keyboard.disableScroll(false);
    }); */
  }
  ionViewDidLoad() {
    this.storage.get('domain').then((value) => {
      this.domain = value;
      this.getDiscussion();
    });

    //Socket communication.
    this.socket.connect();
    this.socket.on('connect_failed', function () {
      console.log('Connection Failed');
    });
    this.socket.on('connect', function () {
        console.log('Connected');
    });
    this.socket.on('notify-discussion', () => this.getNewDiscussion());
    this.socket.on('disconnect', function () {
        console.log('Disconnected');
    });
    
  }

  getNewDiscussion(){
    if(this.discussions.length > 0){
        var body = 'AuthKey='+this.userInfo.user_key+'&_getActivitiesDiscussionByProject=1&activity_id='+this.activity.activity_id+"&last_comment_id="+this.discussions[this.discussions.length-1].discussion_id;
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        this.http
          .get('https://'+this.domain+'.izeberg.com/maxis_osp/api/activities?'+body,{
              headers: headers
            })
            .map(res => res.json())
            .subscribe(data => {
                if(typeof(this.discussions) != 'undefined' && this.discussions.length > 0){
                  this.discussions = this.discussions.concat(data.response.data);
                  this.scrollToBottom();
                }else{
                  this.discussions = data.response.data;
                }
            }, error => {
                console.log(JSON.stringify(error.json()));
            });
    }else{
      this.getDiscussion();
    }
  }

  getDiscussion(){
    var body = 'AuthKey='+this.userInfo.user_key+'&_getActivitiesDiscussionByProject=1&activity_id='+this.activity.activity_id;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    this.http
      .get('https://'+this.domain+'.izeberg.com/maxis_osp/api/activities?'+body,{
          headers: headers
        })
        .map(res => res.json())
        .subscribe(data => {
            this.discussions = data.response.data;
            /* if(typeof(this.discussions) != 'undefined' && this.discussions.length > 0){
              this.discussions = this.discussions.concat(data.response.data);
              this.scrollToBottom();
            }else{
            } */
        }, error => {
            console.log(JSON.stringify(error.json()));
        });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  getUserPic(user_id){
    let user =  this.systemUsers.filter(user => user.user_id === user_id)[0];
    if(typeof(user) != 'undefined'){
      return user.user_image_link;
    }else{
      return 'http://cloud.izeberg.com/maxis_osp/themes/core/assets/images/default.jpeg';
    }
  }

  getUser(user_id){
    let user =  this.systemUsers.filter(user => user.user_id === user_id)[0];
    if(typeof(user) != 'undefined'){
      return user;
    }else{
      return '';
    }
  }

  rewardPoints: any;
  //Complete the task
  completed(){
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    var body = '';
    this.completeButtonDisabled =true;
    this.vibrate.vibrate(100);
    if(!this.isComplete){       
      this.isComplete = true;
      body = 'AuthKey='+this.userInfo.user_key+
                    '&progress_activity_id='+this.activity.activity_id+
                    '&progress_description=Activity Closed'+
                    '&progress_module_id=19'+
                    '&progress_progress=0'+
                    '&progress_status=2'+
                    '&_addActivityProgress=1';      

      this.http.post('https://'+this.domain+'.izeberg.com/maxis_osp/api/activities',
      body, {
      headers: headers
      })
      .map(res => res.json())
      .subscribe(data => {
        if(!data.response.error){   
          this.activityStatus = 2;
          this.getUserInfo();
          this.rewardPoints = data.response.rewardPoints;
          this.showToast(this.rewardPoints);
          this.completeButtonDisabled =false;
        }
      }, error => {
        console.log(JSON.stringify(error.json()));
      });
    }else{         
      this.isComplete = false;
      body = 'AuthKey='+this.userInfo.user_key+
                  '&progress_activity_id='+this.activity.activity_id+
                  '&progress_description=Activity Opened'+
                  '&progress_module_id=19'+
                  '&progress_progress=0'+
                  '&progress_status=1'+
                  '&_addActivityProgress=1';

      this.http.post('https://'+this.domain+'.izeberg.com/maxis_osp/api/activities',
          body, {
          headers: headers
          })
          .map(res => res.json())
          .subscribe(data => {
            if(!data.response.error){ 
              this.activityStatus = 1;
              this.getUserInfo();
              this.completeButtonDisabled =false;
            }
          }, error => {
          console.log(JSON.stringify(error.json()));
          });
    }
  }


  showToast(rewardPoints: string){
    this.modalCtrl.create(ModalToastPage,{rewardPoints: rewardPoints}).present();
  }

  popover: any;
  openAssignee(ev: any){
    console.log(ev);
    this.popover = this.popCtrl.create(TaskAssigneePage,
                                    {
                                      activity_assigned_to_id: this.activity.activity_assigned_to_id, 
                                      systemUsers: this.systemUsers,
                                      this_activity: this
                                    },
                                    { cssClass: "maxHeight"}
                                  );
    this.popover.onDidDismiss(value => {
      if (value) {
        this.activity.activity_assigned_to_id = value;
      }
    });
    this.popover.present({
      ev: ev
    });
  }

  changeAssignee(assigned_to: any){
    this.activity.activity_assigned_to_id = assigned_to;
    this.popover.dismiss();
    this.updateActivity('assigneeUpdate',assigned_to);
  }

  updateProject(){
    this.activity.activity_project_id = this.activity_project_id;
    this.updateActivity("dateUpdate");
  }

  updateDate(dateType: string){
    if(dateType == 'startDate'){
      this.activity.activity_start_datetime = moment(this.startDate).format("YYYY-MM-DD HH:mm:ss");
      this.updateActivity("dateUpdate");
      this.activity.start = this.startDate;
    }else if(dateType == 'endDate'){
      this.activity.activity_end_datetime = moment(this.endDate).format("YYYY-MM-DD HH:mm:ss");
      this.updateActivity("dateUpdate");
      this.activity.end = this.endDate;
    }
  }

  updateActivity(event: any, value = ''){    
    console.log();
    if(event){
      var eventInfo = '';
      if(event == 'assigneeUpdate'){
          eventInfo = '_event='+event+'&activity_assigned_to_id='+value;
      }else if(event == 'dateUpdate'){
          eventInfo = '_event='+event+
                      '&activity_end_datetime='+this.activity.activity_end_datetime+
                      '&activity_start_datetime='+this.activity.activity_start_datetime+
                      '&activity_project_id='+this.activity.activity_project_id;
      } 

      var body = 'AuthKey='+this.userInfo.user_key+
                '&'+eventInfo+                
                '&activity_id='+this.activity.activity_id+
                '&_saveNewActivityFromProject=0';

      var headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');

      this.http.post('https://'+this.domain+'.izeberg.com/maxis_osp/api/activities',
                body, {
                  headers: headers
                })
                .map(res => res.json())
                .subscribe(data => {
                  console.log(data);
                  if(!data.response.error){
                    
                  }
                }, error => {
                  console.log(JSON.stringify(error.json()));
                });
    }
  }

  getProjects(){    
    var body = 'AuthKey='+this.userInfo.user_key+'&_getIonicProject=1';
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    this.http
      .get('https://'+this.domain+'.izeberg.com/maxis_osp/api/projects?'+body,{
          headers: headers
        })
        .map(res => res.json())
        .subscribe(data => {
            this.projects = data.response.data;
            data.response.data.forEach( projects => {
              if(projects.project_start_date){
                projects.project_start_date = moment(projects.project_start_date, "YYYY-MM-DD").format("DD MMM, YYYY");
                projects.project_end_date = moment(projects.project_end_date, "YYYY-MM-DD").format("DD MMM, YYYY");
              }
            });
        }, error => {
            console.log(JSON.stringify(error.json()));
        });
  }

  getUserInfo(){
    var body = 'AuthKey='+this.userInfo.user_key+'&_getLoggedInUserDetails=1';
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    this.http
      .get('https://'+this.domain+'.izeberg.com/maxis_osp/api/users?'+body,{
          headers: headers
        })
        .map(res => res.json())
        .subscribe(data => {
            this.userData.saveUserInfo(data.response.data);
        }, error => {
            console.log(JSON.stringify(error.json()));
        });
  }

  //Discussions
  sendMsg(){
    console.log('um here');
    if (this.discussionMsg != '') {
      var body = 'AuthKey='+this.userInfo.user_key+
                '&_addActivityDiscussion=1'+
                '&record_id='+this.activity.activity_id+
                '&comments='+this.discussionMsg.replace(/\r?\n/g, '<br />');
      var headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      

      this.http.post('https://'+this.domain+'.izeberg.com/maxis_osp/api/activities',
          body, {
            headers: headers
          })
          .map(res => res.json())
          .subscribe(data => {
            console.log(JSON.stringify(data));
              if(!data.response.error){
                //this.discussions[this.discussions.length-1].activityDiscussionText = this.discussionMsg;
                this.socket.emit('update-discussion', '');
                this.discussionMsg = '';
              }
          }, error => {
              console.log(JSON.stringify(error.json()));
          });
    }
  }


  //Discussions
  sendAttachment(){
    //console.log(this.base64Image);
    if (this.base64Image != '') {
      /* var body = 'AuthKey='+this.userInfo.user_key+
                  '&_addActivityDiscussion=1'+
                  '&record_id='+this.activity.activity_id+
                  '&attachImage='+this.base64Image; */
      console.log(this.base64Image);
      var body = 'AuthKey='+this.userInfo.user_key+
                  '&_addActivityDiscussion=1'+
                  '&record_id='+this.activity.activity_id+
                  '&attachImage='+this.base64Image;
      var headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');

      this.http.post('https://'+this.domain+'.izeberg.com/maxis_osp/api/activities',
          body, {
            headers: headers
          })
          .map(res => res.json())
          .subscribe(data => {
            console.log(JSON.stringify(data));
              if(!data.response.error){
                //this.discussions[this.discussions.length-1].activityDiscussionText = this.discussionMsg;
                this.socket.emit('update-discussion', '');
                this.discussionMsg = '';
              }
          }, error => {
              console.log(JSON.stringify(error));
          });
    }
  }

  onFocus(){    
    /* this.showEmojiPicker = true;*/
    alert('um here');
    this.content.resize();
    this.scrollToBottom(); 
  }
  
  onBlur(){    
    /* this.showEmojiPicker = false;*/
    this.content.resize();
    this.scrollToBottom(); 
  }

  scrollToBottom() {
      setTimeout(() => {
          if (this.content._scroll) {
              this.content.scrollToBottom();
          }
      }, 400)
  }

  //upload multimedia
  getMultimediaMenu(){
    this.actionSheet.create({
      buttons: [
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            this.actionHandler(2);
            //this.takePicture();
          }
        },
        {
          text: 'Photo Library',
          icon: 'images',
          handler: () => {
            this.actionHandler(1);
            //this.selectPicture();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    }).present();
  }

  public base64Image: string;
  takePicture(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 500,
      targetHeight: 500,
      allowEdit: true,
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.sendAttachment();
    }, (err) => {
      // Handle error
      console.log(err);
    });
  }

  selectPicture(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: 0,
      targetWidth: 300,
      targetHeight: 300,
      allowEdit: true,
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.sendAttachment();
    }, (err) => {
      // Handle error
      console.log(err);
    });
  }

  imageChosen: any = 0;
  imagePath: any;
  imageNewPath: any;

  actionHandler(selection: any) {
        var options: any;
     
        if (selection == 1) {
          options = {
            quality: 75,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 500,
            targetHeight: 500,
            saveToPhotoAlbum: false
          };
        } else {
          options = {
            quality: 75,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 500,
            targetHeight: 500,
            saveToPhotoAlbum: false
          };
        }
     
        this.camera.getPicture(options).then((imgUrl) => {
          //console.log(imgUrl);
          //var sourceDirectory = imgUrl.substring(0, imgUrl.lastIndexOf('/') + 1);
          var sourceFileName = imgUrl.substring(imgUrl.lastIndexOf('/') + 1, imgUrl.length);
          sourceFileName = sourceFileName.split('?').shift();
          //console.log(sourceFileName+"<->"+sourceDirectory);
          //console.log(sourceFileName);
          console.log();
          /* this.file.copyFile(sourceDirectory, sourceFileName, this.file.externalApplicationStorageDirectory, sourceFileName)            
              .then((result: any) => {
                this.imagePath = imgUrl;
                this.imageChosen = 1;
                this.imageNewPath = result.nativeURL;
                console.log('url: '+this.imagePath);
         
              }, (err) => {
                console.log(JSON.stringify(err));
              }); */
     
        }, (err) => {
          alert(JSON.stringify(err))
        });
     
      }
}
