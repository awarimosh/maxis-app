import {Component, ViewChild, ElementRef} from '@angular/core';
import {
  IonicPage,
  NavController,
  ModalController,
  NavParams,
  PopoverController,
  LoadingController,
  Platform
} from 'ionic-angular';
import {UserData} from '../../providers/user-data';
import {Storage} from '@ionic/storage';
import {Http, Headers} from '@angular/http';
import {ManholeUpdatePage} from '../manhole-update/manhole-update';
import {ActivityFilterPage} from '../activity-filter/activity-filter';
import {global} from "../../assets/global";
import {Geolocation} from '@ionic-native/geolocation';
import {ScreenOrientation} from '@ionic-native/screen-orientation';

import moment from 'moment';
import 'moment/locale/pt-br';

/**
 * Generated class for the ProjectDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google

@IonicPage({
  name: "manhole-detail-page"
})
@Component({
  selector: 'page-manhole-detail',
  templateUrl: 'manhole-detail.html',
})
export class ManholeDetailPage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  projectDetail: any;
  project_id: any;
  project_name: any;
  projectActivities: any;
  userInfo: any;
  systemUsers: any;
  domain: string;
  structList: any;
  actSegment = "structure";
  SLD_total_range_loading: boolean = false;
  SLD_total_range: any = [];
  activeSLD: boolean = false;
  SLDActualData: any;
  activeSLDData: any;
  graphData: any;
  graph_loading: boolean = false;
  public civilChartOptions: any;
  public civilChartLabels: string[];
  public civilChartType: string = 'bar';
  public civilChartLegend: boolean = true;
  public civilChartData: any[];
  totalEOT: number = 0;
  totalSI: number = 0;
  totalCivilPlanned: number = 0;
  totalCivilActual: number = 0;
  totalCablePlanned: number = 0;
  totalCableActual: number = 0;
  public showCivil = true;
  public doughnutChartLabels: string[];
  public doughnutChartData: number[];
  public doughnutChartType: string = 'pie';
  public doughnutChartOptions: any = {
    pieceLabel: {
      render: 'percentage',
      precision: 2
    }
  };
  public showCable = true;
  public cableDoughnutChartLabels: string[];
  public cableDoughnutChartData: number[];
  public cableDoughnutChartType: string = 'pie';
  public cableDoughnutChartOptions: any = {
    pieceLabel: {
      render: 'percentage',
      precision: 2
    }
  };

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public popCtrl: PopoverController,
              public http: Http,
              public userData: UserData,
              public storage: Storage,
              public loading: LoadingController,
              public geo: Geolocation,
              public screenOrientation: ScreenOrientation,
              public platform: Platform) {
    this.project_id = navParams.get('project_id');
    this.project_name = navParams.get('project_name');

    storage.get('userInfo').then((value) => {
      this.userInfo = value;
    });
    storage.get('systemUsers').then((value) => {
      this.systemUsers = value;
    });
    platform.registerBackButtonAction(() => {

      if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.screenOrientation.unlock();
      }
    }, 1);
  }

  ionViewDidLoad() {
    this.storage.get('domain').then((value) => {
      this.domain = value;
      this.getProject();
      this.getActivities();
      this.userData.updateLocal();
      this.loadMap();
    });
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      this.screenOrientation.unlock();
    }
  }

  loadMap() {
    /* var geocoder = new google.maps.Geocoder();
    geocoder = geocoder; */
    this.geo.getCurrentPosition().then(async () => {
      // console.log(JSON.stringify(resp));
      if (!this.SLD_total_range_loading)
        await this.loadSLDActualData();

      let latLng = new google.maps.LatLng('3.100244', '101.735391'); //3.100244, 101.735391

      let styledMap = new google.maps.StyledMapType(global.mapStyle, {name: 'Styled Map'});

      let mapOptions = {
        center: latLng,
        zoom: 16,
        //mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControlOptions: {
          mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
            'styled_map'],
          position: google.maps.ControlPosition.BOTTOM_LEFT
        }
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      //Associate the styled map with the MapTypeId and set it to display.
      this.map.mapTypes.set('styled_map', styledMap);
      this.map.setMapTypeId('styled_map');

      // console.log('this.structList',this.structList);

      let latLangPos = new google.maps.LatLng(
        this.structList[0].site_instruction_detail_lat.replace("N", ""),
        this.structList[0].site_instruction_detail_lng.replace("E", ""));
      this.map.setCenter(latLangPos);
      var infowindow = new google.maps.InfoWindow({content: ''});
      var i = 0;
      let previousPos: any;
      this.structList.forEach(v => {
        if (v.site_instruction_detail_type != 'distance') {
          let ASLDByActivityID = this.isActualSLDByActivityID(v.site_instruction_detail_id);
          var manholesPath;
          if (ASLDByActivityID != false && v.site_instruction_detail_lat.length > 0 && v.site_instruction_detail_lng.length > 0) {

            latLangPos = new google.maps.LatLng(
              v.site_instruction_detail_lat.replace("N", ""),
              v.site_instruction_detail_lng.replace("E", "")
            );
            let marker = new google.maps.Marker({
              position: latLangPos,
              label: "",
              map: this.map,
              title: v.site_instruction_detail_title,
              icon: "assets/img/green-dot.png",
            });
            marker.addListener('click', function () {
              infowindow.setContent("<h4>" + v.site_instruction_detail_title + "</h4><p>" + v.site_instruction_detail_code + "</p>");
              infowindow.open(this.map, this);
            });
            if (i) {
              //Drawing polyline
              manholesPath = new google.maps.Polyline({
                path: [previousPos, latLangPos],
                geodesic: true,
                strokeColor: '#008000',
                strokeOpacity: 1.0,
                strokeWeight: 3
              });
              manholesPath.setMap(this.map);
            }
            previousPos = latLangPos;
            i++;
          }
          else if(v.site_instruction_detail_lat.length > 0 && v.site_instruction_detail_lng.length > 0) {
            latLangPos = new google.maps.LatLng(
              v.site_instruction_detail_lat.replace("N", ""),
              v.site_instruction_detail_lng.replace("E", "")
            );
            let marker = new google.maps.Marker({
              position: latLangPos,
              label: "",
              map: this.map,
              title: v.site_instruction_detail_title,
              icon: "assets/img/red-dot.png",
            });
            marker.addListener('click', function () {
              infowindow.setContent("<h4>" + v.site_instruction_detail_title + "</h4><p>" + v.site_instruction_detail_code + "</p>");
              infowindow.open(this.map, this);
            });
            if (i) {
              //Drawing polyline
              manholesPath = new google.maps.Polyline({
                path: [previousPos, latLangPos],
                geodesic: true,
                strokeColor: '#0084ff',
                strokeOpacity: 1.0,
                strokeWeight: 3
              });
              manholesPath.setMap(this.map);
            }
            previousPos = latLangPos;
            i++;
          }
        }
      });
    }).catch(() => {
      //me.geoLoc = 'Error getting location';
    });

  }

  isActualSLDByActivityID: any = function (id) {
    var found = this.SLDActualData.filter(d => d.activity_a == id);
    var returnV = false;
    if (found.length) {
      returnV = found[0].location;
    }
    // console.log('returnV',returnV);
    return returnV;
  }

  getManholesByActID = function (id) {
    // console.log('this.moduleRecordData.manholes', JSON.stringify(this.moduleRecordData.manholes));
    var found = this.moduleRecordData.manholes.filter(d => d.site_instruction_detail_id == id)
    var returnV = false;
    if (found.length) {
      returnV = found[0];
    }
    return returnV;
  }

  async loadSLD() {
    this.SLD_total_range_loading = true;
    await this.loadSLDActualData();
    this.SLD_total_range.clear;
    var elements = [];
    this.projectDetail.manholes.forEach(element => {
      element.site_instruction_detail_distances = typeof (element.site_instruction_detail_distances) == 'string' ? JSON.parse(element.site_instruction_detail_distances) : element.site_instruction_detail_distances;
      if (elements.length < 6) {
        elements.push(element);
      } else {
        this.SLD_total_range.push(elements);
        elements = [];
        elements.push(element);
      }
    });
    this.SLD_total_range.push(elements);
    elements = [];
  }

  loadSLDDistance = function (data) {
    return JSON.parse(data.site_instruction_detail_distances);
  }

  loadSLDActualData = function () {
    var body = 'AuthKey=' + this.userInfo.user_key;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    const url = this.userData.base_url + 'api/projects/' + '?' + body + '&_getSLDActualData=1&project_id=' + this.project_id;
    // console.log('loadSLDActualData', url);
    this.http.get(url, {
      headers: headers
    })
      .map(res => res.json())
      .subscribe(data => {
        if (data != undefined) {
          this.SLDActualData = data.response.data;
          // console.log('SLDActualData', this.SLDActualData);
          this.SLD_total_range_loading = false;
        }
      }, error => {
        this.SLD_total_range_loading = false;
        console.log('SLDActualData error', JSON.stringify(error));
      });
  }

  setActualSLDByActivityID = function (id) {
    if (this.SLDActualData != null && this.SLDActualData.length > 0) {
      var found = this.SLDActualData.filter(d => d.activity_a == id);
      var returnV = false;
      // var returnV1 = false;
      if (found.length) {
        // returnV1 = found;
        if (found[0].progress != undefined && found[0].progress['GV'] != undefined) {
          returnV = found[0].progress;
        }
      }
      this.activeSLD = returnV;
      // console.log('activeSLD',JSON.stringify(this.activeSLD));
      // this.activeSLDData = returnV1;
      return returnV;
    } else {
      this.activeSLD = false;
      return false;
    }
  }

  getActualSLDByActivityID: any = function (id) {
    if (this.SLDActualData != null && this.SLDActualData.length > 0) {
      var found = this.SLDActualData.filter(d => d.activity_a == id);
      var returnV = false;
      if (found.length) {
        returnV = found[0];
      }
      else {
        console.log('id not found', id);
        console.log('found', found);
      }
      this.activeSLDData = returnV;
      return returnV;
    }
  }

  async loadGraph() {
    this.graph_loading = true;
    let graphData = await this.loadGraphData();
    if (graphData != undefined) {
      if (this.graphData.civil != undefined) {
        try {
          let civil = this.graphData.civil;
          this.civilChartOptions = {
            responsive: true,
            animation: {
              onComplete: function () {
                var chartInstance = this.chart,
                  ctx = chartInstance.ctx;

                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function (dataset, i) {
                  var meta = chartInstance.controller.getDatasetMeta(i);
                  meta.data.forEach(function (bar, index) {
                    var data = dataset.data[index];
                    ctx.fillText(data + 'm', bar._model.x, bar._model.y - 5);
                  });
                });
              }
            }

          };
          this.civilChartType = 'bar';
          this.civilChartLegend = true;

          // this.civilChartLabels = ['CW', 'GV', 'HDD'];
          // this.civilChartData = [
          //   {
          //     data: [parseInt(civil.planned.CW), parseInt(civil.planned.GV), parseInt(civil.planned.HDD)],
          //     label: 'Planned'
          //   },
          //   {data: [parseInt(civil.actual.CW), parseInt(civil.actual.GV), parseInt(civil.actual.HDD)],
          //     label: 'Actual'}
          // ];
          this.civilChartLabels = [];
          this.civilChartData = [
            {
              data: [],
              label: 'Planned'
            },
            {
              data: [],
              label: 'Actual'
            }
          ];
          for (var j = 0; j < Object.keys(civil.planned).length; j++) {
            try {
              var key = Object.keys(civil.planned)[j];
              this.civilChartLabels.push(key);
              this.civilChartData[0].data.push(parseInt(civil.planned[key]));
              this.civilChartData[1].data.push(parseInt(civil.actual[key]));
            }
            catch (Ex) {
              console.log('key', key);
              console.log('civilChartLabels', this.civilChartLabels);
              console.log('civilChartData', this.civilChartData);
              console.log('planned', civil.planned[key]);
              console.log('actual', civil.actual[key]);
            }
          }
          this.graph_loading = false;
          this.totalCivilPlanned = parseInt(civil.planned.CW) + parseInt(civil.planned.GV) + parseInt(civil.planned.HDD);
          this.totalCivilActual = parseInt(civil.actual.CW) + parseInt(civil.actual.GV) + parseInt(civil.actual.HDD);

          this.doughnutChartLabels = ["Pending", "Actual"];
          this.doughnutChartData = [this.totalCivilPlanned - this.totalCivilActual, this.totalCivilActual];
          this.showCivil = true;
        }
        catch (exp) {
          this.userData.presentToast('Civil Data Unavailable');
          this.showCivil = false;
          console.log('showCivil ' + this.showCivil, exp);
        }

        try {
          this.cableDoughnutChartLabels = ["Pending", "Actual"];
          let cable = this.graphData.cable;
          this.totalCablePlanned = parseInt(cable.planned.Coil) + parseInt(cable.planned.distance);
          this.totalCableActual = parseInt(cable.actual.Coil) + parseInt(cable.actual.distance);
          this.cableDoughnutChartData = [this.totalCablePlanned - this.totalCableActual, this.totalCableActual];
          this.showCable = true;
        }
        catch (exp) {
          this.userData.presentToast('Cable Data Unavailable');
          this.showCable = false;
          console.log('showCable ' + this.showCable, exp);
        }
      }
      this.totalEOT = graphData.total_eot;
      this.totalSI = graphData.total_si;
    }
  }

  async loadGraphData() {
    // console.log('load graph');
    var body = 'AuthKey=' + this.userInfo.user_key;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    const url = this.userData.base_url + 'api/projects/?project_id=' + this.project_id + "&_getDashboardStats=1&" + body;
    console.log('load graph', url);
    await new Promise(async (resolve, reject) => {
      this.http.get(this.userData.base_url + 'api/projects/?project_id=' + this.project_id + "&_getDashboardStats=1&" + body, {
        headers: headers
      })
        .map(res => res.json())
        .subscribe(data => {
          if (data != undefined) {
            this.graphData = data.response.data;
            var url = 'data:text/json;charset=utf8,' + encodeURIComponent(JSON.stringify(this.graphData));
            console.log('graphData', url);
          }
          resolve();
        }, error => {
          console.log(JSON.stringify(error));
          reject();
        });
    })
    return this.graphData;
  }

  getProject() {
    var body = 'AuthKey=' + this.userInfo.user_key;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    this.http.get(this.userData.base_url + 'api/projects/' + this.project_id + '?' + body, {
      headers: headers
    })
      .map(res => res.json())
      .subscribe(data => {
        if (data != undefined) {
          this.projectDetail = data.response.data;
          this.structList = this.projectDetail.manholes;
        }
        // console.log('projectDetail', this.projectDetail);
        // console.log(this.projectDetail.project_tools_data.users.data);
      }, error => {
        console.log(JSON.stringify(error));
      });
  }

  loadViewValues() {
    var body = 'AuthKey=' + this.userInfo.user_key;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    const url = this.userData.base_url + 'api/projects/' + this.project_id + '?_getViewData=1' + body;
    console.log('getViewDat', url);
    this.http.get(url, {
      headers: headers
    })
      .map(res => res.json())
      .subscribe(data => {
        console.log('loadViewValues error', JSON.stringify(data));
      }, error => {
        console.log('loadViewValues error', JSON.stringify(error));
      });
  }

  getActivities() {
    var body = 'AuthKey=' + this.userInfo.user_key + '&_getActivitiesByProject=1&project_id=' + this.project_id;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    let loading = this.loading.create({
      content: 'Loading Sites...'
    });

    loading.present();

    this.http.get(this.userData.base_url + 'api/activities/?' + body, {
      headers: headers
    })
      .map(res => res.json())
      .subscribe(data => {
        if (data != undefined && data.response.data) {
          data.response.data.forEach(activity => {
            if (activity.progress) {
              activity.checked = (activity.progress[0].progress_status == 1) ? false : true;
            }
            activity.progress_status = activity.progress ? activity.progress[0].progress_status : false;
            if (activity.activity_end_datetime) {
              activity.endDate = moment(activity.activity_end_datetime, "YYYY-MM-DD HH:mm:ss").format("DD MMM, YYYY")
              activity.end = moment(activity.activity_end_datetime, "YYYY-MM-DD HH:mm:ss").format("YYYY-MM-DD")
            }
            if (activity.activity_start_datetime) {
              activity.startDate = moment(activity.activity_start_datetime, "YYYY-MM-DD HH:mm:ss").format("DD MMM, YYYY")
              activity.start = moment(activity.activity_start_datetime, "YYYY-MM-DD HH:mm:ss").format("YYYY-MM-DD")
            } else {
              activity.startDate = activity.endDate;
              activity.start = activity.end;
            }
          });
          this.projectActivities = data.response.data;
        }
        loading.dismiss();
      }, error => {
        console.log(JSON.stringify(error));
      });
  }

  /* openDetail(activity: any){

    console.log(this.projectDetail);
    this.navCtrl.push(ActivitiesDetailPage,{activity: activity, project_id: this.project_id, projectDetail: this.projectDetail});
  } */

  openDetail(activity: any, status: any) {
    //let obj = arr.find(o => o.name === 'string 1');
    // let siteA = this.structList.find(itm => itm.site_instruction_detail_id === activity.activity_site_ins_detail_a);
    // let siteB = this.structList.find(itm => itm.site_instruction_detail_id === activity.activity_site_ins_detail_b);
    let siteA = this.structList.find(itm => itm.site_instruction_detail_id === (parseInt(activity.activity_site_ins_detail_a) + 1).toString());
    let siteB = this.structList.find(itm => itm.site_instruction_detail_id === (parseInt(activity.activity_site_ins_detail_b) + 1).toString());
    this.navCtrl.push(ManholeUpdatePage, {
      siteA: siteA,
      siteB: siteB,
      activity: activity,
      status: status,
      project_id: activity.activity_project_id,
      myActivities: this.projectActivities,
      callBack: this.updateStatus
    });

  }

  updateStatus(myActivities: any, returnActivity: any, data: any) {
    this.projectActivities = myActivities;
    this.projectActivities.forEach(element => {
      if (element.activity_id === returnActivity) {
        element.progress_status = data;
      }
    });
  }

  getUserPic(user_id) {
    if (user_id != null) {
      let user = this.systemUsers.filter(user => user.user_id === user_id)[0];
      if (typeof (user) != 'undefined') {
        return user.user_image_link;
      } else {
        // return 'http://cloud.izeberg.com/maxis_osp/themes/core/assets/images/default.jpeg';
        return 'https://apps.izeberg.com/maxis_osp/themes/core/assets/images/default.jpeg';
      }
    } else {
      // return 'http://cloud.izeberg.com/maxis_osp/themes/core/assets/images/default.jpeg';
      return 'https://apps.izeberg.com/maxis_osp/themes/core/assets/images/default.jpeg';
    }
  }

  getAssignedName(user_id) {
    let user = this.systemUsers.filter(user => user.user_id === user_id)[0];
    if (typeof (user) != 'undefined') {
      return user.user_name;
    } else {
      return 'Yet to assign';
    }
  }

  segmentChanged(event: any) {
    console.log('Selected: ', event._value);
    if (event._value == 'Map') {
      this.loadMap();
    }
    else if (event._value == 'SLD') {
      this.loadSLD();
    }
    else if (event._value == 'Graph') {
      this.loadGraph();
    }
    else if (event._value == 'EOT') {
      this.loadEOT();
    } else if (event._value == 'DC') {
      this.loadDC();
    }

    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      if (event._value == 'SLD') {
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
      }
      else {
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.screenOrientation.unlock();
      }
    }
  }

  deliverableChecklist: any = {
    etp_radio: 'yes',
    etp_no_permit_remarks: "",
    etp_permit_application_date: "",
    etp_permit_approval_date: "",
    itp_radio: 'yes',
    itp_no_permit_remarks: "",
    itp_permit_application_date: "",
    itp_permit_approval_date: "",
    deliverables_work_start_date: "",
    deliverables_e2e_date: "",
    deliverables_joint_site_acceptance_date: "",
    deliverables_submission_date: "",
    deliverables_ncr_upload_date: "",
    deliverables_cpc_date: ""
  };

  loadDC() {
    var body = 'AuthKey=' + this.userInfo.user_key + "&_getDeliverablesByProject=1&project_id=" + this.project_id;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    this.http.get(this.userData.base_url + 'api/projects?' + body, {
      headers: headers
    })
      .map(res => res.json())
      .subscribe(data => {
        if (data != undefined) {
          data = data.response.data;
          // console.log('data', data);
          this.deliverableChecklist = {
            etp_radio: data.deliverable_etp_no_permit_remarks ? 'yes' : 'no',
            etp_no_permit_remarks: data.deliverable_etp_no_permit_remarks ? data.deliverable_etp_no_permit_remarks : '',
            etp_permit_application_date: data.deliverable_etp_permit_application_date,
            etp_permit_approval_date: data.deliverable_etp_permit_approval_date,
            itp_radio: data.deliverable_itp_no_permit_remarks ? 'yes' : 'no',
            itp_no_permit_remarks: data.deliverable_itp_no_permit_remarks ? data.deliverable_itp_no_permit_remarks : '',
            itp_permit_application_date: data.deliverable_itp_permit_application_date,
            itp_permit_approval_date: data.deliverable_itp_permit_approval_date,
            deliverables_work_start_date: data.deliverable_work_start_date,
            deliverables_e2e_date: data.deliverable_e2e_date,
            deliverables_joint_site_acceptance_date: data.deliverable_joint_site_acceptance_date,
            deliverables_submission_date: data.deliverable_submission_date,
            deliverables_ncr_upload_date: data.deliverable_ncr_upload_date,
            deliverables_cpc_date: data.deliverable_cpc_date
          };
          // console.log("============================");
          // console.log('um' + data.deliverable_ncr_upload_date);
          // console.log("============================");
        }
      }, error => {
        console.log(JSON.stringify(error));
      });
  }

  createDC(values: any) {
    values = values;
    console.log('createDC', JSON.stringify(this.deliverableChecklist));
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    this.deliverableChecklist.etp_radio = this.deliverableChecklist.etp_radio == "yes" ? 1 : 0;
    this.deliverableChecklist.itp_radio = this.deliverableChecklist.itp_radio == "yes" ? 1 : 0;


    var body = 'AuthKey=' + this.userInfo.user_key +
      '&deliverableChecklist=' + JSON.stringify(this.deliverableChecklist) +
      '&project_id=' + this.project_id +
      '&_saveProjectDeliverables=1' +
      '&_appPost=1';

    this.http.post(this.userData.base_url + 'api/projects',
      body, {
        headers: headers
      })
      .map(res => res.json())
      .subscribe(data => {
        if (data != undefined && !data.response.error) {
          console.log(data.response.data);
        }
      }, error => {
        console.log(JSON.stringify(error));
      });
  }

  eotDetail: any;

  loadEOT() {
    var body = 'AuthKey=' + this.userInfo.user_key + "&_getEotByProject=1&project_id=" + this.project_id;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    this.http.get(this.userData.base_url + 'api/eot/?' + body, {
      headers: headers
    })
      .map(res => res.json())
      .subscribe(data => {
        if (data != undefined) {
          this.eotDetail = data.response.data;
          console.log('loadEOT', this.eotDetail)
        }
        ;
      }, error => {
        console.log(JSON.stringify(error));
      });
  }

  today: any;
  m: any;

  getActivityDueDateCheck(item) {
    var check = '0';
    if (item.endDate) {
      this.m = moment(item.endDate, "DD MMM, YYYY"); // or whatever start date you have
      this.today = moment().startOf('day');
      var days = Math.round((this.today - this.m) / 86400000);
      if (days == 0) {
        check = 'priority-high';
      } else if (days > 0 && item.checked === false) {
        check = 'priority-urgent';
      } else if (days > 0 && item.checked === true) {
        check = 'priority-ok';
      } else if (days < 0) {
        check = 'priority-ok';
      } else if (days < 0) {
        check = '';
      }
    }
    return check;
  }

  activityFilter = 1;

  openFilter(ev: any) {
    let popover = this.popCtrl.create(ActivityFilterPage,
      {activityFilter: this.activityFilter, projectDetail: this});
    popover.onDidDismiss(value => {
      if (value) {
        this.activityFilter = value;
      }
    });
    popover.present({
      ev: ev
    });
  }

  changeFilter(filterVal: any) {
    this.activityFilter = filterVal;
  }

  dateFormat(date: any, format: any) {
    return moment(date).format(format);
  }

  setLang(lang: any) {
    return lang;
  }
}
