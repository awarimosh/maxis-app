import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { UserData } from '../../providers/user-data';
import { EotCreatePage } from '../eot-create/eot-create';
import { WorkflowPage } from '../workflow/workflow';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';


@IonicPage()
@Component({
  selector: 'page-eot',
  templateUrl: 'eot.html',
})
export class EotPage {
  userInfo: any;
  records: any;
  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public http: Http,
    public storage: Storage,
    public userData: UserData,
    public modalCtrl: ModalController
  ) {
  }

  ionViewDidLoad() {
    this.storage.get('userInfo').then((value) => {
      if (value) {
        this.userInfo = value;
        this.getRecords();
      }
    });
  }

  getRecords() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    var body = 'AuthKey=' + this.userInfo.user_key +
      '&_draw=1' +
      '&_layout="list"' +
      '&_pageLimit=100' +
      '&_pageNo=0' +
      '&_search="[]"';

    this.http.post(this.userData.base_url + 'api/eot',
      body, {
        headers: headers
      })
      .map(res => res.json())
      .subscribe(data => {
        if (!data.response.error) {
          this.records = data.response.data.allData;
          // var url = 'data:text/json;charset=utf8,' + encodeURIComponent(JSON.stringify(this.records));
          // console.log('records', url);
        }
      }, error => {
        console.log(JSON.stringify(error.json()));
      });
  }

  create(record: any = null) {
    let createEOT = this.modalCtrl.create(EotCreatePage, { record: record });

    createEOT.onDidDismiss(data => {
      data = data;
      this.getRecords();
    });
    createEOT.present();
  }

  workflow(module_id: any, record_id: any) {
    let workflow = this.modalCtrl.create(
      WorkflowPage,
      {
        module_id: module_id,
        record_id: record_id
      }
    );
    workflow.onDidDismiss(data => {
      data = data;
      this.getRecords();
    });
    workflow.present();
  }

}
