import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { UserData } from '../../providers/user-data';
import 'rxjs/add/operator/map';

// import moment from 'moment';
// import 'moment/locale/pt-br';
@IonicPage()
@Component({
  selector: 'page-eot-create',
  templateUrl: 'eot-create.html',
})
export class EotCreatePage {
  formKey: any = "eot";
  moduleAttributesList: any;
  moduleSettings: any;
  moduleData: any;
  userInfo: any;
  record: any;
  eot_title: any;
  eot_description: any;
  eot_original_date: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public userData: UserData,
    public storage: Storage,
    public viewCtrl: ViewController,
    public http: Http,
  ) {
    this.moduleAttributesList = this.userData.moduleSettings[this.formKey].module_settings.attributesList[0];
    this.moduleSettings = this.userData.moduleSettings[this.formKey].module_settings;
    this.moduleData = this.userData.moduleSettings[this.formKey].module_data;
    this.record = navParams.get('record');
    if (this.record) {
      this.eot_title = this.record.eot_title;
      this.eot_description = this.record.eot_description;
      if (this.record.eot_orignal_date != undefined || this.record.eot_orignal_date != null) {
        // this.eot_original_date = moment(this.record.eot_orignal_date, "YYYY-MM-DD").format("dd mm yyyy")
        this.eot_original_date = this.record.eot_orignal_date;
      }
      this.setValues();
    }
    console.log('record', JSON.stringify(this.record));
  }

  setValues() {
    this.moduleAttributesList.forEach(element => {
      element.model = this.record[element.attrKey];
      // console.log(this.record[element.attrKey]);
    });
  }

  selectDropdown(data) {
    var url = 'data:text/json;charset=utf8,' + encodeURIComponent(JSON.stringify(data));
    console.log('selectDropdown', url);
  }

  ionViewDidLoad() {
    this.storage.get('userInfo').then((value) => {
      if (value) {
        this.userInfo = value;
      }
    });
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  createEot(form: any) {
    console.log(JSON.stringify(form));
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    var body = 'AuthKey=' + this.userInfo.user_key +
      '&eot_title=' + form.eot_title +
      '&eot_description=' + form.eot_description +
      '&custom_127=' + form.custom_127 +
      '&custom_128=' + form.custom_128;
    if (this.record) {
      body += '&eot_id=' + this.record.eot_id;
    }

    this.http.post(this.userData.base_url + 'api/eot',
      body, {
        headers: headers
      })
      .map(res => res.json())
      .subscribe(data => {
        if (!data.response.error) {
          this.closeModal();
          //console.log(data.response.data);
        }
      }, error => {
        console.log(JSON.stringify(error.json()));
      });
  }

  getModel(elmModel: any) {
    if (this.record) {
      return true;
    } else {
      return elmModel;
    }
  }
}
