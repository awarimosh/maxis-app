import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { IonicPage, NavController, Platform, ToastController, MenuController } from 'ionic-angular';

import { UserData } from '../../providers/user-data';

import { UserOptions } from '../../interfaces/user-options';

import { TabsPage } from '../tabs-page/tabs-page';
import { SignupPage } from '../signup/signup';
import { TouchID } from '@ionic-native/touch-id';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';


@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'login.html'
})
export class LoginPage {
  // login: UserOptions = { username: 'admin@integriocorp.com', password: '123123123', siteName: 'apps' };
  login: UserOptions = { username: '', password: '', siteName: 'apps' };
  submitted = false;
  userInfo: any;
  device_id: any;
  constructor(
              public navCtrl: NavController,
              public userData: UserData,
              public http: Http,
              public toastCtrl: ToastController,
              public platform: Platform,
              public menu: MenuController,
              public touchID: TouchID,
              private uniqueDeviceID: UniqueDeviceID
              ) {
      this.menu.enable(false);
      /* touchID.verifyFingerprint('Scan your fingerprint please')
                .then(
                  res => alert('Ok'+JSON.stringify(res)),
                  err => alert('Error'+JSON.stringify(err))
                ); */
  }


  ionViewDidLoad() {;
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.uniqueDeviceID.get()
      .then((uuid: any) => {
        this.device_id = uuid;
        // this.userData.presentToast("device_id :: " + this.device_id);
        console.log('device_id',this.device_id);
      })
      .catch((error: any) => console.log(error));
    }
  }

  otherLogin(type: any,loginForm: any){
    if(type == 1){
      this.login.username = 'tariq@integrioinc.com';
      this.login.password = '123123123';
    }else if(type == 2){
      this.login.username = 'sayem@integrioinc.com';
      this.login.password = '123123123';
    }else if(type == 3){
      this.login.username = 'pankaj@integriocorp.com';
      this.login.password = '123123123';
    }
    this.onLogin(loginForm);
  }

  onLogin(form: NgForm) {
    this.submitted = true;
    if (form.valid) {
      var body = 'login_email='+this.login.username+'&login_password='+this.login.password+
      // '&_userLogin=1';
     '&device_id=' + this.device_id +'&_userLogin=1&_mobile=1';
      var headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');

      this.http
        .post('https://'+this.login.siteName+'.izeberg.com/maxis_osp/api/users',
          body, {
            headers: headers
          })
          .map(res => res.json())
          .subscribe(data => {
              if(!data.response.error){
                console.log(data.response.data);
                this.userData.login(data.response.data.user_name);
                this.userData.saveUserInfo(data.response.data);
                this.userData.saveUserEmail(this.login.username);
                this.userInfo = data.response.data;

                this.userData.saveDomain(this.login.siteName);
                this.getAllSystemUsers();
                this.navCtrl.push(TabsPage);
                console.log(data.response.data);
              }else{
                const msg = data.response.msg;
                let toast = this.toastCtrl.create({
                  message: 'Failed : ' + msg.toString(),
                  duration: 3000,
                  position: 'top'
                });
                toast.present();
              }
          }, error => {
              console.log('https://'+this.login.siteName+'.izeberg.com/maxis_stag/api/users');
              console.log('err',JSON.stringify(error.json()));
          });
    }
  }

  onSignup() {
    this.navCtrl.push(SignupPage);
  }

  getAllSystemUsers(){
    var body = 'AuthKey='+this.userInfo.user_key+'&_getUserPublicDetails=1';
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    this.http
      .get('https://'+this.login.siteName+'.izeberg.com/maxis_osp/api/users/?'+body,{
          headers: headers
        })
        .map(res => res.json())
        .subscribe(data => {
          this.userData.saveSystemUsers(data.response.data);
          console.log(data.response.data);
        }, error => {
            console.log(JSON.stringify(error.json()));
        });
  }
}
