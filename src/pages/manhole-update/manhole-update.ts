import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform, LoadingController } from 'ionic-angular';
import { DatePipe } from '@angular/common';
import { UpdateFormPage } from '../update-form/update-form';
import { UpdateViewPage } from '../update-view/update-view';
import { UserData } from '../../providers/user-data';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import moment from 'moment';
import 'moment/locale/pt-br';
/**
 * Generated class for the ManholeUpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-manhole-update',
  templateUrl: 'manhole-update.html',
})
export class ManholeUpdatePage {
  actSegment = 'civil';
  activity: any;
  pointA: any;
  pointB: any;
  siteA: any;
  siteB: any;
  current: number = 20;
  max: number = 100;
  userInfo: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public datePipe: DatePipe,
    public userData: UserData,
    public sqlite: SQLite,
    public platform: Platform,
    public loading: LoadingController,
    public http: Http,
    public storage: Storage
  ) {
    storage.get('userInfo').then((value) => {
      this.userInfo = value;
    });
    this.activity = navParams.get('activity');
    this.siteA = navParams.get('siteA');
    this.siteB = navParams.get('siteB');
    // var url = 'data:text/json;charset=utf8,' + encodeURIComponent(JSON.stringify(this.activity));
    // console.log('data', url);
    this.pointA = this.activity.activity_site_ins_detail_a_cont;
    this.pointB = this.activity.activity_site_ins_detail_b_cont;
    // var url = 'data:text/json;charset=utf8,' + encodeURIComponent(JSON.stringify(this.activity));
    // console.log('activity', url);
  }

  showForm() {
    this.userData.syncCustomForm();
  }

  ionViewDidLoad() {
    // this.userData.updateLocal();
    this.getCustomFormUpdate();
  }

  showdetail(data) {
    // console.log('detail data', data);
    let updateForm = this.modalCtrl.create(UpdateViewPage, { updateData: data });
    updateForm.onDidDismiss(async data => {
      data = data;
      await this.getCustomFormUpdate();
    });
    updateForm.present();
  }

  getCustomFormUpdate() {
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.sqlite.create(this.userData.sqlConf)
        .then((db: SQLiteObject) => {
          //data insert section
          db.executeSql("SELECT * FROM customForm WHERE form_project_id=? " + "AND form_activity_id=? ORDER BY form_created_on DESC",
            [this.activity.activity_project_id, this.activity.activity_id])
            .then((result) => {
              this.activity.activity_update_local = [];
              if (result.rows.length > 0) {
                for (var i = 0; i < result.rows.length; i++) {
                  if (this.activity.activity_update_local.length == 0) {
                    this.activity.activity_update_local.push(result.rows.item(i));
                  } else {
                    if (result.rows.item(i).form_custom_fields != null) {
                      for (var j = 0; j < this.activity.activity_update_local.length; j++) {
                        if (result.rows.item(i).form_id == this.activity.activity_update_local[j].form_id) {
                          break;
                        }
                        else if (result.rows.item(i).form_created_on == this.activity.activity_update_local[j].form_created_on) {
                          break;
                        }
                        else if (j == this.activity.activity_update_local.length - 1) {
                          this.activity.activity_update_local.push(result.rows.item(i));
                        }
                      }
                    }
                  }
                }
              }
              // var url = 'data:text/json;charset=utf8,' + encodeURIComponent(JSON.stringify(this.activity.activity_update_local));                
              // console.log('all form data', url);
            }).catch(e => console.log('error: 1' + JSON.stringify(e)));
        }).catch(e => console.log('error: 2' + JSON.stringify(e)));
    }
  }

  removeCustomForm(id: string) {
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.sqlite.create(this.userData.sqlConf)
        .then((db: SQLiteObject) => {
          //data insert section
          db.executeSql("delete from customForm where form_id='"+id+"'",null)
            .then((result) => {
              console.log('delete url ' + id, JSON.stringify(result));
            }).catch(e => console.log('error: 2' + JSON.stringify(e)));
        }).catch(e => console.log('error: 4' + JSON.stringify(e)));
    }
  }

  dateFormat(date: any, format: any) {
    return moment(date).format(format);
  }

  getCivilCompletedPercentage(id: any, f: any, cFields: any) {
    let disComplete = this.getFields(f, cFields);
    disComplete = this.getDistance(cFields);
    let percentageCompleted: any = 0;
    let plannedDistance: any = this.siteB.site_instruction_detail_cable_distances;
    if (disComplete == 0) {
      percentageCompleted = 0;
    }
    else {
      if (plannedDistance == null || plannedDistance == 0) {
        percentageCompleted = '-';
      }
      else {
        percentageCompleted = ((disComplete / plannedDistance) * 100).toFixed(2);
        if(percentageCompleted > 100)
        percentageCompleted = 100;
      }
    }
    if (percentageCompleted > 50) {
      let progress_cont = <HTMLElement>document.getElementsByClassName('pie-chart-' + id)[0];
      progress_cont.className += " gt-50";
    }

    let civil_progress = <HTMLElement>document.getElementsByClassName('civil-progress-' + id)[0];
    percentageCompleted = parseInt(percentageCompleted);
    if (typeof (percentageCompleted) == 'number') {
      let deg: any = 360 * percentageCompleted / 100;
      civil_progress.style.transform = 'rotate(' + deg + 'deg)';
    } else {
      civil_progress.style.transform = 'rotate(' + 0 + 'deg)';
    }
    return percentageCompleted;
  }

  getCableCompletedPercentage(id: any, f: any, cFields: any) {
    let disComplete = this.getFields(f, cFields);
    disComplete = this.getCableDistance(cFields);
    let percentageCompleted: any = 0;
    let plannedDistance: any = this.siteB.site_instruction_detail_cable_distances;
    if (disComplete == 0) {
      percentageCompleted = 0;
    }
    else {
      if (plannedDistance == null || plannedDistance == 0) {
        percentageCompleted = '-';
      }
      else {
        percentageCompleted = ((disComplete / plannedDistance) * 100).toFixed(2);
        if(percentageCompleted > 100)
        percentageCompleted = 100;
      }
    }
    if (percentageCompleted > 50) {
      let progress_cont = <HTMLElement>document.getElementsByClassName('pie-chart-' + id)[0];
      progress_cont.className += " gt-50";
    }

    let cable_progress = <HTMLElement>document.getElementsByClassName('cable-progress-' + id)[0];
    percentageCompleted = parseInt(percentageCompleted);
    if (typeof (percentageCompleted) == 'number') {
      let deg: any = 360 * percentageCompleted / 100;
      cable_progress.style.transform = 'rotate(' + deg + 'deg)';
    } else {
      cable_progress.style.transform = 'rotate(' + 0 + 'deg)';
    }
    return percentageCompleted;
  }

  segmentChanged(event: any) {
    console.log(event._value);
  }

  openForm(formKey: any, name: any) {
    let updateForm = this.modalCtrl.create(
      UpdateFormPage,
      { formKey: formKey, name: name, activity: this.activity }
    );
    updateForm.onDidDismiss(data => {
      data = data;
      this.getCustomFormUpdate();
    });
    updateForm.present();
  }

  deleteItem(form: any, name: any) {
    switch (name) {
      case 'civil':
        this.deleteAPI("civil/" + form.form_id + "/", form);
        break;
      case 'cable':
        this.deleteAPI("cable/" + form.form_id + "/", form);
        break;
      case 'clearance':
        this.deleteAPI("site_clearance/" + form.form_id + "/", form);
        break;
    }
  }

  deleteAPI(type: string, form: any) {
    var body = 'AuthKey=' + this.userInfo.user_key;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let loading = this.loading.create({
      content: 'Delete ...'
    });
    loading.present();

    const url = this.userData.base_url + 'api/' + type + "/" + form.form_id + '?' + body;
    console.log('url',url);
    this.http.delete(url, {
      headers: headers
    })
      .map(res => res.json())
      .subscribe(data => {
        if (!data.error) {
          this.removeCustomForm(form.form_id);
          this.getCustomFormUpdate();
        }
        console.log('delete response', JSON.stringify(data));
        loading.dismiss();
      }, error => {
        this.userData.presentToast("failed");
        // this.removeCustomForm(form.form_id);
        // this.getCustomFormUpdate();
        loading.dismiss();
        console.log('error', JSON.stringify(error));
      });
  }

  getFields(f: any, cFields: any) {
    cFields = JSON.parse(cFields);
    return cFields[f] ? cFields[f] : 0;
  }

  getDistance(cFields: any) {
    cFields = JSON.parse(cFields);
    let distance = 0;
    const a = cFields.custom_151 ? parseInt(cFields.custom_151) : 0;
    const b = cFields.custom_153 ? parseInt(cFields.custom_153) : 0;
    const c = cFields.custom_155 ? parseInt(cFields.custom_155) : 0;
    const d = cFields.custom_158 ? parseInt(cFields.custom_158) : 0;
    const e = cFields.custom_160 ? parseInt(cFields.custom_160) : 0;
    distance = a + b + c + d + e;
    return distance;
  }

  getCableDistance(cFields: any) {
    cFields = JSON.parse(cFields);
    let distance = 0;
    const a = cFields.custom_164 ? parseInt(cFields.custom_164) : 0;
    distance = a;
    return distance;
  }

  //This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
  calcCrow(lat1, lon1, lat2, lon2) {
    const R = 6371; // km
    const dLat = this.toRad(lat2 - lat1);
    const dLon = this.toRad(lon2 - lon1);
    const alat1 = this.toRad(lat1);
    const alat2 = this.toRad(lat2);

    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(alat1) * Math.cos(alat2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c;
    return d;
  }

  // Converts numeric degrees to radians
  toRad(Value) {
    return Value * Math.PI / 180;
  }

}
