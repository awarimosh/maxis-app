import { Component, ElementRef, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { UploadfileProvider } from '../../providers/uploadfile/uploadfile';
import { File } from '@ionic-native/file';

import { Http, Headers } from '@angular/http';
//import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
/**
 * Generated class for the TpUploadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tp-upload',
  templateUrl: 'tp-upload.html',
})
export class TpUploadPage {
  userInfo: any;
  domain: any;
  form: FormGroup;
  loading: boolean = false;
  @ViewChild('fileInput') fileInput: ElementRef;
  constructor(
              public userData: UserData,
              public navCtrl: NavController, 
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public uploadFile: UploadfileProvider,
              public file: File,
              public loader: LoadingController,
              public fb: FormBuilder,
              public valid: Validators,
              public http: Http,
            ) {
      this.createForm();
  }

  checkValid(){
    console.log(this.form.valid);
  }

  createForm() {
    this.form = this.fb.group({
      technical_proposal_title: ['', Validators.required],
      technical_proposal_description: ['', Validators.required],
      custom_70: null,
      custom_147: null
    });
  }

  ionViewDidLoad() {
    this.userData.getUserInfo().then((value) => {
      if(value){
        this.userInfo = value;
      }      
    });
  }


  closeModal(){
    this.viewCtrl.dismiss();
  }

  uploadTp(event){
    event = event;
    /* let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.form.get('custom_70').setValue({
          filename: file.name,
          filetype: file.type,
          value: reader.result.split(',')[1]
        })
      };
    } */
  }

  private prepareSave(): any {
    let input = new FormData();
    input.append('technical_proposal_title', this.form.get('technical_proposal_title').value);
    input.append('technical_proposal_description', this.form.get('technical_proposal_description').value);
    input.append('custom_70', this.form.get('custom_70').value);
    return input;
  }

  onSubmit() {
    const formModel = this.prepareSave();
    this.loading = true;
    console.log(JSON.stringify(formModel));
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    var body = 'AuthKey='+this.userInfo.user_key+
                    '&technical_proposal_data='+JSON.stringify(formModel)+
                    '&_tp_create_app=1';

    this.http.post(this.userData.base_url+'api/technical_proposal',
      body, {
      headers: headers
      })
      .map(res => res.json())
      .subscribe(data => {
        if(!data.response.error){   
          //this.closeModal();
          //console.log(data.response.data);
        }
      }, error => {
        console.log(JSON.stringify(error.json()));
      });
  }

  clearFile() {
    this.form.get('tp').setValue(null);
    this.fileInput.nativeElement.value = '';
  }
}
