import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProjectDetailPage } from '../project-detail/project-detail';

/**
 * Generated class for the ActivityFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activity-filter',
  templateUrl: 'activity-filter.html',
})
export class ActivityFilterPage {
  private projectDetail: ProjectDetailPage;
  activityFilter: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.projectDetail = navParams.get('projectDetail');
    this.activityFilter = navParams.get('activityFilter');
    this.projectDetail.changeFilter(this.activityFilter);    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivityFilterPage');
  }

  applyFilter(){
    console.log(this.activityFilter);
    //console.log(this.projectDetail);
    this.projectDetail.changeFilter(this.activityFilter);
  }

}
