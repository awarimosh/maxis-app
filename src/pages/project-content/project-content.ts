import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

//import moment from 'moment';
//import 'moment/locale/pt-br';

//declare var google
/**
 * Generated class for the ProjectContentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-project-content',
  templateUrl: 'project-content.html',
})
export class ProjectContentPage {

  project: any;
  userInfo: any;
  domain: string;

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  marker: any;
  formKey: any="projects";
  moduleAttributesList: any;
  moduleSettings: any;
  moduleData: any;
  constructor(public userData: UserData,
            public navCtrl: NavController, 
            public navParams: NavParams,
            public http: Http,
            public storage: Storage, 
          ) {
    this.project = navParams.get('project');
    this.moduleAttributesList = this.userData.moduleSettings[this.formKey].module_settings.attributesList[0];   
    // console.log("moduleAttributesList",JSON.stringify(this.moduleAttributesList));
    this.moduleSettings = this.userData.moduleSettings[this.formKey].module_settings;    
    this.moduleData = this.userData.moduleSettings[this.formKey].module_data;
  }

  ionViewDidLoad() {  
    this.storage.get('userInfo').then((value) => {
      if(value){
        this.userInfo = value;
        this.setValues(); 
      }      
    });
  }

  setValues(){
    this.moduleAttributesList.forEach(element => {
      element.model = this.project[element.attrKey];
      // console.log(element.attrKey,this.project[element.attrKey]);
    });
  }

  getVal(key: any, data: any){
    let val = data.filter(data=>data.key == key);
    var arrVal = Object.keys(val).map(function(key) {
                    return val[key].value;
                  });
    return arrVal[0];
  }

  getProjectsDetail(){    
    var body = 'AuthKey='+this.userInfo.user_key;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    this.http
      .get(this.userData.base_url+'api/projects/?'+body,{
          headers: headers
        })
        .map(res => res.json())
        .subscribe(data => {
            this.project = data.response.data;
            
        }, error => {
            console.log(JSON.stringify(error.json()));
        });
  }

}
