import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the SiteInstructionAddNodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-site-instruction-add-node',
  templateUrl: 'site-instruction-add-node.html',
})
export class SiteInstructionAddNodePage {

  nodeType: any;
  typeName: any;
  methods: any = [];
  method: any;
  distance: any;
  extra: any;
  constructor(
              public navCtrl: NavController, 
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public datePicker: DatePicker,
              public geo: Geolocation
            ) {
    this.nodeType = navParams.get('nodeType');
    this.typeName = navParams.get('typeName');
    this.extra = navParams.get('extra');
    this.extra = JSON.parse(this.extra);
    this.extra.site_instruction_detail_type = this.nodeType;
    console.log(this.extra);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SiteInstructionAddNodePage');
  }

  closeModal(){
    this.viewCtrl.dismiss();
  }

  addMethod(){
    this.methods.push({"method": this.method, "value": this.distance});
    this.method = '';
    this.distance = '';
    console.log(this.methods);
  }

  removeMethod(key: any){
    this.methods.splice(key, 1);
  }

  save(){
    if(this.nodeType == 'distance'){
      var methods = JSON.stringify(this.methods);
      methods = methods.replace(/\\/g, '');
      let data = {
        "site_instruction_detail_form_id": "0",
        "site_instruction_detail_tp_id": "",
        "site_instruction_detail_title": this.extra.nodeATitle+" - "+this.extra.nodeBTitle,
        "site_instruction_detail_code": null,
        "site_instruction_detail_type": "distance",
        "site_instruction_detail_order": "1",
        "site_instruction_detail_lat": null,
        "site_instruction_detail_lng": null,
        "site_instruction_detail_new": "1",
        "site_instruction_detail_description": null,
        "site_instruction_detail_dateOfIssue": null,
        "site_instruction_detail_effectiveDate": null,
        "site_instruction_detail_distance": null,
        "site_instruction_detail_distances": methods
      };
      this.viewCtrl.dismiss(data);
    }else{
      this.viewCtrl.dismiss(this.extra);
    }
  }

  getLocation(){
    this.geo.getCurrentPosition()
        .then((resp) => {
          console.log(JSON.stringify(resp));
          this.extra.site_instruction_detail_lat = "N"+resp.coords.latitude;
          this.extra.site_instruction_detail_lng = "E"+resp.coords.longitude;
        });
  }
}