import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';
import { UploadfileProvider } from '../../providers/uploadfile/uploadfile';
import { TakePhotoProvider } from '../../providers/take-photo/take-photo';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import 'rxjs/add/operator/map';
import moment from 'moment';
// import moment, { lang } from 'moment';
import 'moment/locale/pt-br';
import { AlertController } from 'ionic-angular';
import { PhotoViewer } from '@ionic-native/photo-viewer';
/**
 * Generated class for the UpdateFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-update-form',
  templateUrl: 'update-form.html',
})
export class UpdateFormPage {
  formKey: any;
  name: any;
  userInfo: any;
  moduleAttributesList: any;
  moduleSettings: any;
  moduleData: any;
  moduleIds = { 'civil': 103, "cable": 104, "e2e": 105, "site_clearance": 106 };
  hasImage: boolean = false;
  activity: any;
  domain: string;
  sqlConf: any;
  // image: any = [
  //   { "name": "Image 1", "model": 'image 1', "path": 'path 1' },
  // ];
  image: any = [];
  gpsCoordinate: any = {
    'lat': '',
    'lng': ''
  }
  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public userData: UserData,
    public storage: Storage,
    public camera: Camera,
    public platform: Platform,
    public http: Http,
    public uploadFile: UploadfileProvider,
    public capturePhoto: TakePhotoProvider,
    public file: File,
    public sqlite: SQLite,
    public alertCtrl: AlertController,
    public photoViewer: PhotoViewer
  ) {
    //form Key
    this.formKey = navParams.get('formKey');
    this.name = navParams.get('name');
    this.activity = navParams.get('activity');

    this.moduleAttributesList = this.userData.moduleSettings[this.formKey].module_settings.attributesList[0];
    if (this.moduleAttributesList != undefined && this.moduleAttributesList.length > 0) {
      var url = 'data:text/json;charset=utf8,' + encodeURIComponent(JSON.stringify(this.moduleAttributesList));
      console.log('module Attributes List', url);
      this.moduleAttributesList.forEach(element => {
        if (element.type == 'fileUpload' || element.type == 'multiFileUpload') {
          element.model = null;
          element.data = [];
        }
      });
    }
    this.moduleSettings = this.userData.moduleSettings[this.formKey].module_settings;

    this.moduleData = this.userData.moduleSettings[this.formKey].module_data;
    this.storage.get('userInfo').then((value) => {
      if (value) {
        this.userInfo = value;
      }
    });

    //Database settings
    if (this.platform.is('ios')) {
      this.sqlConf = {
        name: 'maxis.db',
        location: 'default'
      };
    }
    else {
      this.sqlConf = {
        name: 'maxis.db',
        iosDatabaseLocation: 'Documents'
      };
    }
  }

  ionViewDidLoad() {
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  saveLocalForm(form: any) {
    let formID = this.moduleIds[this.formKey];
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.sqlite.create(this.sqlConf)
        .then((db: SQLiteObject) => {
          //data insert section
          db.executeSql("INSERT INTO customForm (form_module_key, form_module_id, form_project_id, form_activity_id, form_custom_fields, form_created_on, form_created_by, form_deleted) " +
            "VALUES(?,?,?,?,?,?,?,?)",
            [this.formKey, formID, this.activity.activity_project_id, this.activity.activity_id,
            JSON.stringify(form), moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
            this.userInfo.user_id, 0])
            .then(() => {
              console.log('inserted successfully Executed SQL');
              this.userData.syncCustomFormOne(formID);
              this.closeModal();
              return;
            })
            .catch(e => {
              let alert = this.alertCtrl.create({
                title: 'Alert 1',
                subTitle: JSON.stringify(e),
                buttons: ['OK']
              });
              alert.present();
            })
        })
        .catch(e => {
          let alert = this.alertCtrl.create({
            title: 'Alert 2',
            subTitle: JSON.stringify(e),
            buttons: ['OK']
          });
          alert.present();
        })
    }
    else {
      let alert = this.alertCtrl.create({
        title: 'Not mobile Web',
        subTitle: 'Not mobile Web',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  showConfirm(form: any) {
    let confirm = this.alertCtrl.create({
      title: 'Create',
      message: 'You are about to create ' + this.formKey.toString() + " form",
      buttons: [
        {
          text: 'ok',
          handler: () => {
            // console.log('form', JSON.stringify(form));
            if (this.hasImage)
              this.uploadOnce(form);
            else
              this.saveLocalForm(form);
            this.hasImage = false;
          }
        },
        {
          text: 'cancel',
          handler: () => {
            console.log('form', JSON.stringify(form));
            this.userData.presentToast("cancel clicked");
          }
        }
      ]
    });
    confirm.present();
  }

  update(form: any) {
    var body = 'AuthKey=' + this.userInfo.user_key;
    body += '&' + this.moduleData.module_key + '_project_id=' + this.activity.activity_project_id;
    body += '&' + this.moduleData.module_key + '_activity_id=' + this.activity.activity_id;
    body += '&form_data=' + JSON.stringify(form);
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post(this.moduleSettings.remoteAPI,
      body, {
        headers: headers
      })
      .map(res => res.json())
      .subscribe(data => {
        console.log('post img', data);
        this.closeModal();
      }, error => {
        console.log(JSON.stringify(error.json()));
      });

  }

  takePhoto(key: any, cusAttr: any) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      targetWidth: 200,
      targetheight: 150,
      sourceType: this.camera.PictureSourceType.CAMERA,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    this.camera.getPicture(options).then((imagePath) => {
      this.hasImage = true;
      // Special handling for Android library
      var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1).toString().replace(/\.[^/.]+$/, "") + ".jpg";
      var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
      if (this.image.length == 1 && this.image[0].name == "Image 1") {
        this.image[0].name = key;
        this.image[0].model = currentName;
        this.image[0].path = correctPath + currentName;
      } else {
        this.image.push({ "name": key, "model": currentName, "path": correctPath + currentName });
      }
      cusAttr.model = correctPath + currentName;
      // console.log('>>>>>>>>>',correctPath + currentName);
      cusAttr.data.push(correctPath + currentName);
    }, (err) => {
      this.userData.presentToast('Cancelled' + err.message);
    });
  }

  uploadOnce(form: any) {
    var ImagePromise = new Promise(async (resolve) => {
      let res = await this.uploadFile.uploadImageWithPathOnce(this.image, form);
      if (res != undefined) {
        console.log('res 1', JSON.stringify(res));
        resolve(res);
      }
    });

    ImagePromise.then((data: any) => {
      console.log('now save', JSON.stringify(data));
      this.saveLocalForm(data);
      return null;
    }).catch((err) => {
      console.log('err', JSON.stringify(err));
    });
  }

  showImage(url: string) {
    console.log('url', url);
    this.photoViewer.show(url);
  }

  public uploadAll(form: any) {
    const promisesArray: any[] = [];
    if (this.hasImage) {
      this.image.forEach(v => {
        if (v.model) {
          promisesArray.push(new Promise(async (resolve) => {
            let res = '';
            res = await this.uploadFile.uploadImageWithPath(v.name, v.model, v.path);
            if (res.length > 0) {
              let json = {
                name: v.name,
                res: res
              }
              console.log('||||||||json', JSON.stringify(json));
              resolve(json);
            }
          }).then((data: any) => {
            console.log('data>>>>', JSON.stringify(data));
            form[data.name] = data.res;
            this.userData.presentToast(data.name.toString() + ' succesfully uploaded.');
          }).catch(e => {
            console.log('err in upload', JSON.stringify(e));
          }));
        }
      });
      Promise.all(promisesArray)
        .then(() => {
          console.log(">>>> now save");
          return this.saveLocalForm(form);
        }).catch(e => {
          console.log('err', JSON.stringify(e));
        });
    }
    else {
      this.saveLocalForm(form);
    }
  }

  getFormKey() {
    return "moduleMainForm" + this.formKey;
  }

  getLocation(key: any, cusAttr: any) {
    function onSuccess(position) {
      switch (key) {
        case 'custom_176':
          cusAttr.model = position.coords.latitude + '/' + position.coords.longitude;
          break;
        case 'custom_178':
          cusAttr.model = position.coords.latitude + '/' + position.coords.longitude;
          break;
      }
    };

    function onError(error) {
      this.UpdateFormPage.userData.presentToast('No geo : code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
    }
    navigator.geolocation.getCurrentPosition(onSuccess, onError);
  }

  getActiveActivity = function () {
    return this.activeActivity;
  }
}