import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage'
import { UserData } from '../../providers/user-data';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  // Doughnut
  public doughnutChartLabels: string[];
  public doughnutChartData: number[];
  public doughnutChartType: string = 'pie';
  public doughnutChartOptions: any = {
    responsive : true,
    maintainAspectRatio: false,
    legend: {
      position : 'bottom'
    },
    pieceLabel: {
      render: 'percentage',
      precision: 2
    }
  };
  public totalProjects: number;
  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  userInfo: any;
  data: any;
  constructor(public navCtrl: NavController,
    public loading: LoadingController,
    public navParams: NavParams,
    public http: Http,
    public storage: Storage,
    public userData: UserData,
    public modalCtrl: ModalController) {

  }

  ionViewDidLoad() {
    this.storage.get('userInfo').then((value) => {
      if (value) {
        this.userInfo = value;
        this.getData();
      }
    });
  }

  getData() {
    var body = 'AuthKey=' + this.userInfo.user_key + '&_project_overall_widget=1';
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    let loading = this.loading.create({
      content: 'Loading...'
    });

    loading.present();

    this.http
      .get(this.userData.base_url + 'api/projects?' + body, {
        headers: headers
      })
      .map(res => res.json())
      .subscribe(data => {
        this.data = data.response.data;
        if (this.data != null) {
          var labels = [], values = [];
          for (let i = 0; i < Object.keys(this.data).length; i++) {
            let label = Object.keys(this.data)[i].toString();
            if (label == '_TOTAL_PROJECTS') {
              this.totalProjects = parseInt(this.data[label].toString());
              continue;
            }
            values.push(parseInt(this.data[label].toString()));
            label = label.replace(/_/g, ' ');
            label = label.replace(' TOTAL ', '');
            labels.push(label);
          }
        }
        let item = labels.shift();
        labels.push(item);
        item = values.shift();
        values.push(item);
        this.doughnutChartLabels = labels;
        this.doughnutChartData = values;
        console.log('doughnutChartLabels',JSON.stringify(this.doughnutChartLabels));
        console.log('doughnutChartData',JSON.stringify(this.doughnutChartData));
        loading.dismiss();
      }, error => {
        console.log('error', JSON.stringify(error.json()));
        loading.dismiss();
      });
  }

}
