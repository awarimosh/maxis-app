import { Component,NgZone } from '@angular/core';
import { IonicPage, NavController, Platform, ModalController, PopoverController, NavParams } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Http, Headers } from '@angular/http';

import { ActivitiesDetailPage } from '../activities-detail/activities-detail';
import { ActivityFilterPage } from '../activity-filter/activity-filter';
import { LoginPage } from '../login/login';
import {Camera} from '@ionic-native/camera';
//import { File } from '@ionic-native/file';
//import { FileTransfer } from '@ionic-native/file-transfer';
/* import { CreateTaskPage } from "../create-task/create-task"; */

import moment from 'moment';
import 'moment/locale/pt-br';
/**
 * Generated class for the MyTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var ApiAIPromises: any;

@IonicPage()
@Component({
  selector: 'page-my-task',
  templateUrl: 'my-task.html',
})
export class MyTaskPage {

  userInfo: any;
  systemUsers: any;
  domain: any;
  myActivities = [];
  noRecord = false;
  taskFilter = "";
  question="";
  answer="";
  myTask = false;
  actSegment = "tasks";
  izeBot = false;
  botFilter = 'myTask';
  botSpeech: any;
  newTask= false;
  sqlConf: any;
  storageDirectory: string = '';
  downloadedImage: any;
  constructor(
              public navCtrl: NavController, 
              public navParams: NavParams, 
              public modalCtrl: ModalController, 
              public popCtrl: PopoverController, 
              public http: Http,
              public userData: UserData,
              public platform: Platform,
              public ngZone: NgZone,
              public storage: Storage,
              public sqlite: SQLite,
              public camera: Camera,
              //public file: File,
              //public fileTransfer: FileTransfer
            ) {
    if(this.platform.is('ios')){
      this.sqlConf = {
                  name: 'izeberg.db',
                  location: 'default'
                  };
    }
    else{
      this.sqlConf = {
                  name: 'izeberg.db',
                  iosDatabaseLocation: 'Documents'
                  };
    }
    storage.get('userInfo').then((value) => {
      this.userInfo = value;
      if(this.userInfo != null){
        this.storage.get('domain').then((value) => {
          this.domain = value;
          storage.get('systemUsers').then((value) => {
            this.systemUsers = value;
            this.getMyActivities();
          })
          .catch(() => console.log('error loading activities'));
        });
      }else{
        this.navCtrl.setRoot(LoginPage);
      }
    });

    platform.ready().then(() => {
      this.userData.watchNetwork();
      if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
        ApiAIPromises.init({
          clientAccessToken: "07a96d71bbc54419b43228ca613446e5"
        })
        .then((result) =>  console.log(result))
      }
    });

    this.platform.ready().then(() => {
      // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
      if(!this.platform.is('cordova')) {
        return false;
      }

      if (this.platform.is('ios')) {
        //this.storageDirectory = this.file.documentsDirectory;
      }
      else if(this.platform.is('android')) {
        //this.storageDirectory = this.file.dataDirectory;
      }
      else {
        // exit otherwise, but you could add further types here e.g. Windows
        return false;
      }
    });
  }

  ionViewDidLoad() {
    
  }

  imageChosen: any = 0;
  imagePath: any;
  imageNewPath: any;

  takePhoto(selection: any){
    var options: any;
     
        if (selection == 1) {
          options = {
            quality: 75,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 500,
            targetHeight: 500,
            saveToPhotoAlbum: false
          };
        } else {
          options = {
            quality: 75,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 500,
            targetHeight: 500,
            saveToPhotoAlbum: false
          };
        }
     
        this.camera.getPicture(options).then((imgUrl) => {
          //console.log(imgUrl);
          //var sourceDirectory = imgUrl.substring(0, imgUrl.lastIndexOf('/') + 1);
          var sourceFileName = imgUrl.substring(imgUrl.lastIndexOf('/') + 1, imgUrl.length);
          sourceFileName = sourceFileName.split('?').shift();
          //console.log(sourceFileName+"<->"+sourceDirectory);
          //console.log(sourceFileName);
          console.log();
          /* this.file.copyFile(sourceDirectory, sourceFileName, this.storageDirectory, Date.now()+sourceFileName)            
              .then((result: any) => {
                this.imagePath = imgUrl;
                this.imageChosen = 1;
                this.imageNewPath = result.nativeURL;
                console.log(JSON.stringify(result.nativeURL));
                //console.log('url: '+this.imageNewPath);
         
              }, (err) => {
                console.log(JSON.stringify(err));
              }); */
     
        }, (err) => {
          alert(JSON.stringify(err))
        });
  }

  createTask(){
    if(!this.newTask) this.newTask = true;
    else              this.newTask = false;
    
    /* this.navCtrl.push(CreateTaskPage,{referral: 'myTask',
                                                        callBack: this.getMyActivities
                                                      }); */
  }

  doRefresh(refresher) {
    this.getMyActivities();
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
  
  newTaskMsg : string;
  saveTask(type: string){
    var taskType: number;
    switch(type){
      case 'tasks':
                    taskType = 3;
                    break;
      case 'events':
                    taskType = 1;
                    break;
      case 'milestones':
                    taskType = 2;
                    break;
    }
    
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    var body = 'AuthKey='+this.userInfo.user_key+
        '&activity_assigned_to_id='+this.userInfo.user_id+
        '&activity_board_id=0'+
        '&activity_description='+
        '&activity_end_datetime='+moment(new Date()).format("YYYY-MM-DD HH:mm:ss")+
        '&activity_id=0'+
        '&activity_milestone_id=0'+
        '&activity_name='+this.newTaskMsg+
        '&activity_project_id=0'+
        '&activity_start_datetime='+moment(new Date()).format("YYYY-MM-DD HH:mm:ss")+
        '&activity_tags=[]'+
        '&activity_type='+taskType+
        '&activity_type_id='+
        '&_event="titleUpdate"'+
        '&_saveNewActivityFromProject=1';   

        

    this.http.post('https://'+this.domain+'.izeberg.com/maxis_osp/api/activities',
                    body, {
                    headers: headers
                    })
                    .map(res => res.json())
                    .subscribe(data => {
                      if(!data.response.error){          
                        this.newTaskMsg = '';

                        var activity = data.response.data;
                        activity.checked = false;
                        activity.progress_status = 1;
          
                        if (activity.activity_end_datetime){
                            activity.endDate = moment(activity.activity_end_datetime, "YYYY-MM-DD HH:mm:ss").format("DD MMM, YYYY")
                            activity.end = moment(activity.activity_end_datetime, "YYYY-MM-DD HH:mm:ss").format("YYYY-MM-DD")
                        }
                        if (activity.activity_start_datetime){
                            activity.startDate = moment(activity.activity_start_datetime, "YYYY-MM-DD HH:mm:ss").format("DD MMM, YYYY")
                            activity.start = moment(activity.activity_start_datetime, "YYYY-MM-DD HH:mm:ss").format("YYYY-MM-DD")
                        } else{
                            activity.startDate = activity.endDate;
                            activity.start = activity.end;
                        }
                        activity.dueDays = this.getActivityDueDateStatus(activity);
                        activity.progress = [];
                        console.log(activity);
                        this.myActivities.unshift(activity);
                      }
                    }, error => {
                        console.log(JSON.stringify(error.json()));
                    });

  }

  sendVoice() {
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      try {     
        // !!!
        ApiAIPromises.levelMeterCallback(function(level) {
          console.log(level);
        }); 

        ApiAIPromises.setListeningStartCallback(function () {
          console.log("listening started");
        });

        ApiAIPromises.setListeningFinishCallback(function () {
          console.log("listening stopped");
        });
        ApiAIPromises.requestVoice({})
          .then(({result}) => {
            this.ngZone.run(()=> {
              this.answer = result.fulfillment.speech;
              if(result.metadata.intentName == 'create_task'){  
                this.newTaskMsg = this.answer;
                this.saveTask('tasks');      
                this.izeBot = false;
              }else{
                this.botFilter = this.answer;
                this.question = this.answer;
              }              
            });
          });                      
      } catch (e) {
          alert(e);
      }
    }
  }

  askIzeBot(){
    if(!this.izeBot){
      this.izeBot = true;
    }else{
      this.izeBot = false;
      this.botFilter = 'myTask';
    }
  }
  
  ask() {
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      ApiAIPromises.requestText({
        query: this.question
      })
      .then(({result: {fulfillment: {speech}}}) => {
        this.ngZone.run(()=> {
          this.answer = speech;
          this.botFilter = this.answer;
        });
      });
    }
  }

  getMyActivities(){
    //this.myActivities = 
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.sqlite.create(this.sqlConf)
      .then((db: SQLiteObject) => {
          //data insert section
          db.executeSql("SELECT * FROM activity",[])
          .then((result) => {
              this.myActivities = [];
              if(result.rows.length >0 ){
                for(var i = 0; i<result.rows.length; i++){
                  this.myActivities[i] = JSON.parse(result.rows.item(i).activity_object);
                }
              }
              else{
                this.getMyActivitiesRemote();
              }
          })
          .catch(e => console.log(e));
      })
      .catch(e => alert(JSON.stringify(e)));
    }else{
      this.getMyActivitiesRemote();
    }
  }

  getMyActivitiesRemote(){
    var body = 'AuthKey='+this.userInfo.user_key+'&_getMyActivities=1&user_id='+this.userInfo.user_id;
    
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http
      .get('https://'+this.domain+'.izeberg.com/maxis_osp/api/activities?'+body,{
          headers: headers
        })
        .map(res => res.json())
        .subscribe(data => {
          if(data.response.data) {
            data.response.data.forEach( activity => {
              if (activity.progress){
                  activity.checked = (activity.progress[0].progress_status == 1) ? false : true;
              }
              activity.progress_status = activity.progress[0].progress_status;

              if (activity.activity_end_datetime){
                  activity.endDate = moment(activity.activity_end_datetime, "YYYY-MM-DD HH:mm:ss").format("DD MMM, YYYY")
                  activity.end = moment(activity.activity_end_datetime, "YYYY-MM-DD HH:mm:ss").format("YYYY-MM-DD")
              }
              if (activity.activity_start_datetime){
                  activity.startDate = moment(activity.activity_start_datetime, "YYYY-MM-DD HH:mm:ss").format("DD MMM, YYYY")
                  activity.start = moment(activity.activity_start_datetime, "YYYY-MM-DD HH:mm:ss").format("YYYY-MM-DD")
              } else{
                  activity.startDate = activity.endDate;
                  activity.start = activity.end;
              }
              activity.dueDays = this.getActivityDueDateStatus(activity);
              this.saveLocally(activity.activity_assigned_to_id, activity.activity_name,JSON.stringify(activity));
                        
            });
          }
          if(data.response.data){
              this.myActivities = data.response.data;
          }else{
              this.noRecord = true;
          }
        }, error => {
            console.log(JSON.stringify(error.json()));
        });
    
  }


  saveLocally(assigned_id: number, task: string, data: any){
    if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
      this.sqlite.create(this.sqlConf)
        .then((db: SQLiteObject) => {
            //data insert section
            db.executeSql("INSERT INTO activity (activity_assigned_to_id, activity_name, progress_status, dueDays, activity_object, activity_sync) "+
                        "VALUES(?,?,2,4,?,0)",[assigned_id,task,data])
              .then(() => console.log('inserted successfully Executed SQL'))
              .catch(e => console.log('error: '+e));
        })
        .catch(e => alert(JSON.stringify(e)));
    }
  }

  getUserPic(user_id){
    let user =  this.systemUsers.filter(user => user.user_id === user_id)[0];
    if(typeof(user) != 'undefined'){
      return user.user_display_pic;
    }else{
      return 'http://cloud.izeberg.com/maxis_osp/themes/core/assets/images/default.jpeg';
    }
  }

  getAssignedName(user_id){
    let user =  this.systemUsers.filter(user => user.user_id === user_id)[0];
    if(typeof(user) != 'undefined'){
      return user.user_name;
    }else{
      return 'Yet to assign';
    }
  }

  getActivityDueDateStatus(item){
    if (item.end) {
      this.m = moment(item.end, "YYYY-MM-DD"); // or whatever start date you have
      this.today = moment().startOf('day');
      var days = this.m.diff(this.today, 'days');
      if(days == 0){
        return "todayTask";
      }else if(days > 0 && days < 7 && item.checked === false){
        return "endingSoon";
      }else if(days < 0 && item.checked === false){
        return "overdueTask";
      }else if(days < 0 && item.checked === true){
        return "completedTask";
      }else{
        return days;
      }
    }
    
  }

  today: any;
  m: any;
  getActivityDueDateCheck(item){
    var check = '0';

    if (item.end) {
        this.m = moment(item.end, "YYYY-MM-DD"); // or whatever start date you have
        this.today = moment().startOf('day');
        var days = Math.round((this.today - this.m) / 86400000);

        if (days == 0)
        {
            check = 'priority-high';
        } else if (days > 0  && item.checked === false)
        {
            check = 'priority-urgent';
        } else if (days > 0  && item.checked === true)
        {
            check = 'priority-ok';
        } else if (days < 0)
        {
            check = 'priority-ok';
        } else if (days < 0)
        {
            check = '';
        }
    }    
    return check;
  }

  activityFilter = 1;
  openFilter(ev: any){
    let popover = this.popCtrl.create(ActivityFilterPage,
                                    {activityFilter: this.activityFilter, projectDetail: this});
    popover.onDidDismiss(value => {
      if (value) {
        this.activityFilter = value;
      }
    });
    popover.present({
      ev: ev
    });
  }

  changeFilter(filterVal: any){
    this.activityFilter = filterVal;
  }

  openDetail(activity: any, status: any){ 
    this.navCtrl.push(ActivitiesDetailPage,{activity: activity, 
                                            status: status, 
                                            project_id: activity.activity_project_id,
                                            myActivities: this.myActivities,
                                            callBack: this.updateStatus
                                           });
    
  }
  updateStatus(myActivities: any,returnActivity: any, data: any){
    this.myActivities = myActivities;
    if(this.myActivities.length > 0){
      this.myActivities.forEach(element => {
          if(element.activity_id === returnActivity){
            element.progress_status = data;
          }
      });
    }
  }
}
