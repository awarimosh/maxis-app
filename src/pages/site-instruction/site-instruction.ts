import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { UserData } from '../../providers/user-data';
import { SiteInstructionDetailPage } from '../site-instruction-detail/site-instruction-detail';
import { SiteInstructionCreatePage } from '../site-instruction-create/site-instruction-create';
import { WorkflowPage } from '../workflow/workflow';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/**
 * Generated class for the SiteInstructionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-site-instruction',
  templateUrl: 'site-instruction.html',
})
export class SiteInstructionPage {

  userInfo: any;
  records: any;
  constructor(
                public navCtrl: NavController, 
                public loadingCtrl: LoadingController, 
                public navParams: NavParams,
                public http: Http,
                public storage: Storage, 
                public userData: UserData,
                public modalCtrl: ModalController,  
              ) {
  }

  ionViewDidLoad() {
    this.storage.get('userInfo').then((value) => {
      if(value){
        this.userInfo = value;
        this.getRecords(); 
      }      
    }); 
  }

  getRecords(){
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    var body = 'AuthKey='+this.userInfo.user_key+
                    '&_draw=1'+
                    '&_layout="list"'+
                    '&_pageLimit=100'+
                    '&_pageNo=0'+
                    '&_search="[]"';      

    this.http.post(this.userData.base_url+'api/site_instruction_form',
      body, {
      headers: headers
      })
      .map(res => res.json())
      .subscribe(data => {
        if(!data.response.error){   
          this.records = data.response.data.allData;
          console.log(this.records);
        }
      }, error => {
        console.log(JSON.stringify(error.json()));
      });
  }

  loadDetail(record: any){
    this.navCtrl.push(SiteInstructionDetailPage,{
      record: record
    });
  }

  create(){
    //SiteInstructionCreatePage
    let createSIF = this.modalCtrl.create(SiteInstructionCreatePage);
    createSIF.onDidDismiss(data => {
      data = data;
      this.getRecords();
    });
    createSIF.present();
  }

  workflow(module_id: any, record_id: any){
    let workflow = this.modalCtrl.create(
                                        WorkflowPage,
                                        {
                                          module_id: module_id,
                                          record_id: record_id
                                        }
                                      );
    workflow.onDidDismiss(data => {
      data = data;
      this.getRecords();
    });
    workflow.present();
  }
}
