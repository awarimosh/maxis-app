import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Http, Headers } from '@angular/http';

import { ActivitiesDetailPage } from '../activities-detail/activities-detail';
import { ProjectDetailPage } from '../project-detail/project-detail';

/**
 * Generated class for the CalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {
  eventSource;
  viewTitle;
  isToday: boolean;
  calendar = {
      mode: 'month',
      currentDate: new Date()
  }; // these are the variable used by the calendar.
  constructor(
                public navCtrl: NavController, 
                public navParams: NavParams,
                public storage: Storage,
                public http: Http,
                public modalCtrl: ModalController
            ) {
  }

  userInfo: any;
  domain: any;

  ionViewDidLoad() {
    this.storage.get('userInfo').then((value) => {
        this.userInfo = value;
        this.storage.get('domain').then((value) => {
            this.domain = value;
            this.get_events();
        });
    });
  }

  eventRecords: any;
  get_events(){
    var body = 'AuthKey='+this.userInfo.user_key+'&_getEventData=1';
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    this.http
      .get('https://'+this.domain+'.izeberg.com/maxis_osp/api/calendars?'+body,{
          headers: headers
        })
        .map(res => res.json())
        .subscribe(data => {
            this.eventRecords = data.response.data.record;
            this.loadEvents(this.eventRecords);
        }, error => {
            console.log(JSON.stringify(error.json()));
        });
  }

  loadEvents(calEvents: any) {
        var events = [];
        console.log(calEvents);
        calEvents.forEach((data,key) => {
            if(data.event_type == 'project'){
                events.push({
                    title: data.title,
                    startTime: new Date(data.start.replace(' ', 'T')),
                    endTime: new Date(data.end.replace(' ', 'T')),
                    allDay: false,
                    event_type: data.event_type,
                    event_id: data.project_id,
                    event_actual_index: key
                });
            }else if(data.event_type == 'activity-1' || data.event_type == 'activity-2' || data.event_type == 'activity-3'){
                events.push({
                    title: data.title,
                    startTime: new Date(data.start.replace(' ', 'T')),
                    endTime: new Date(data.end.replace(' ', 'T')),
                    allDay: false,
                    event_type: data.event_type,
                    event_id: data.activity_id,
                    event_detail: data.activity_detail
                });
            }
        });
        this.eventSource = events;
    }
    onViewTitleChanged(title) {
        this.viewTitle = title;
    }
    onEventSelected(event) {
        if(event.event_type == 'project'){
            this.navCtrl.push(ProjectDetailPage, {project_id: event.event_id});
        }else if(event.event_type == 'activity-1' || event.event_type == 'activity-2' || event.event_type == 'activity-3'){

            this.navCtrl.push(ActivitiesDetailPage, {
                                                        activity: event.event_detail,
                                                        status: event.event_detail.progress[0].progress_status, 
                                                        project_id: event.event_detail.activity_project_id,
                                                        myActivities: '',
                                                        callBack: this.updateStatus
                                                    });
        }
    }
    updateStatus(myActivities: any,returnActivity: any, data: any){
        console.log(myActivities+returnActivity,data);
        /* this.myActivities = myActivities;
        if(this.myActivities.length > 0){
            this.myActivities.forEach(element => {
                if(element.activity_id === returnActivity){
                element.progress_status = data;
                }
            });
        } */
    }
    changeMode(mode) {
        this.calendar.mode = mode;
    }
    today() {
        this.calendar.currentDate = new Date();
    }
    onTimeSelected(ev) {
        console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
            (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
    }
    onCurrentDateChanged(event:Date) {
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        event.setHours(0, 0, 0, 0);
        this.isToday = today.getTime() === event.getTime();
    }
    onRangeChanged(ev) {
        console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
    }
    markDisabled = (date:Date) => {
        var current = new Date();
        current.setHours(0, 0, 0);
        return date < current;
    };
    
}

