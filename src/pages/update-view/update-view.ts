import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { File } from '@ionic-native/file';
import { PhotoViewer } from '@ionic-native/photo-viewer';

declare let cordova: any;
/**
 * Generated class for the UpdateViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update-view',
  templateUrl: 'update-view.html',
})
export class UpdateViewPage {
  updateData: any;
  forms: any = { 'civil': "Civil Update", 'cable': "Cable Update", "site_clearance": "Site Clearance Update" };
  moduleIds = { 'civil': 103, "cable": 104, "e2e": 105, "site_clearance": 106 };
  moduleAttributesList: any;
  moduleSettings: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public userData: UserData,
    public file: File,
    public photoViewer: PhotoViewer
  ) {
    this.updateData = navParams.get('updateData');
    this.moduleAttributesList = this.userData.moduleSettings[this.updateData.form_module_key].module_settings.attributesList[0];
    this.moduleSettings = this.userData.moduleSettings[this.updateData.form_module_key].module_settings;

    // console.log("================================");
    // var url = 'data:text/json;charset=utf8,' + encodeURIComponent(JSON.stringify(this.moduleAttributesList));
    // console.log('moduleAttributesList',url);
    // console.log("================================");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateViewPage');
    this.setValues();
  }

  setValues() {
    if (this.updateData != null && this.updateData.form_custom_fields != null) {
      let custom_fields = JSON.parse(this.updateData.form_custom_fields);
      // console.log('moduleAttributesList',JSON.stringify(this.moduleAttributesList));
      this.moduleAttributesList.forEach(element => {
        if (element.type == "multiFileUpload") {
          let model = custom_fields[element.attrKey];
          console.log('model', model);
          element.data = model != null ? model.toString().split(',') : '';
        }
        element.model = custom_fields[element.attrKey];
      });
    }
  }

  getDropDown(id: any, element: any) {
    let temp = element.data.filter(d => d.key == id);
    if (temp.length > 0)
      return temp[0].value;
    return '';
  }

  getTitle() {
    return this.forms[this.updateData.form_module_key];
  }

  retrieveImage(name: any) {
    console.log('retrieveImage: ', name);
    this.file.readAsDataURL(cordova.file.externalCacheDirectory, name).then(
      file64 => {
        console.log('file64: ', file64);
        return file64;
      }).catch(err => {
        console.log('err: ', err);
        return null;
      });
  }

  showImage(url: string) {
    this.photoViewer.show(url);
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

}
