import { IonicPage } from 'ionic-angular';
import { Component,NgZone } from '@angular/core';
import {  Platform } from 'ionic-angular';

import { NavParams, NavController, MenuController } from 'ionic-angular';

//import { AboutPage } from '../about/about';
/* import { MapPage } from '../map/map'; */
import { IzeBotPage } from '../ize-bot/ize-bot';
/* import { SchedulePage } from '../schedule/schedule'; */
/* import { SpeakerListPage } from '../speaker-list/speaker-list'; */
//import { CalendarPage } from '../calendar/calendar';
import { ProjectsPage } from '../projects/projects';
import { SiteInstructionPage } from '../site-instruction/site-instruction';
import { EotPage } from '../eot/eot';
//import { MyTaskPage } from '../my-task/my-task';
import { DashboardPage } from '../dashboard/dashboard';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';

//declare var ApiAIPromises: any;

@IonicPage()
@Component({
  templateUrl: 'tabs-page.html'
})
export class TabsPage {
  // set the root pages for each tab
  /* tab1Root: any = MyTaskPage; */
  tab1Root: any = ProjectsPage;
  tab2Root: any = SiteInstructionPage;
  tab3Root: any = IzeBotPage;
  tab4Root: any = EotPage;
  /* tab5Root: any = AboutPage; */
  tab5Root: any = DashboardPage;
  mySelectedIndex: number;
  notListening = true;

  constructor(
              public navParams: NavParams,
              public storage: Storage,
              public navCtrl: NavController,
              public platform: Platform,
              public ngZone: NgZone,
              public menu: MenuController
            ) {
    this.storage.get('hasLoggedIn').then((value) => {
      if(!value){
        this.navCtrl.setRoot(LoginPage)
      }else{
          this.menu.enable(true);
          this.mySelectedIndex = navParams.data.tabIndex || 0;
      }
    });

    /* platform.ready().then(() => {
      if (!this.platform.is('core') && !this.platform.is('mobileweb')) {
        ApiAIPromises.init({
          clientAccessToken: "07a96d71bbc54419b43228ca613446e5"
        })
        .then((result) =>  console.log(result))
      }
    }); */
  }

  answer: any;


}
