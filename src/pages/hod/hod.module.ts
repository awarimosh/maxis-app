import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HodPage } from './hod';

@NgModule({
  declarations: [
    HodPage,
  ],
  imports: [
    IonicPageModule.forChild(HodPage),
  ],
})
export class HodPageModule {}
