import { NgModule } from '@angular/core';
import { CustomFormDirective } from './custom-form/custom-form';
import { MyDirective } from './my/my';
@NgModule({
	declarations: [CustomFormDirective,
    MyDirective],
	imports: [],
	exports: [CustomFormDirective,
    MyDirective]
})
export class DirectivesModule {}
