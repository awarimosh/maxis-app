import { Directive } from '@angular/core';

@Directive({
  selector: '[custom-form]' // Attribute selector
})
export class CustomFormDirective {

  constructor() {
    console.log('Hello CustomFormDirective Directive');
  }

}
